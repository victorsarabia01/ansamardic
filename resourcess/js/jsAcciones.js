function hoverTxtDiente(idTxtDiente)
{
	var idDiente=idTxtDiente.substring(3, 6);
	var css=
	{
		"box-shadow": "0px 0px 10px blue"
	}
	$("#"+idDiente).css(css);
}

function outTxtDiente(idTxtDiente)
{
	var idDiente=idTxtDiente.substring(3, 6);
	var css=
	{
		"box-shadow": "none"
	}
	$("#"+idDiente).css(css);
}

function seleccionarCara(idCaraDiente)
{
	$("#txtCaraTratada").val(idCaraDiente);
}

function seleccionarDiente(idDiente)
{
	$("#txtIdentificadorDienteGeneral").val(idDiente);
	$("#txtDienteTratado").val(idDiente);
}

function agregarTratamiento(diente, cara, estado)
{
	if(diente=="" || cara=="")
	{
		alert("Debe seleccionar el diente y la cara de dicho diente para agregar un Tratamiento");
		return;
	}

	var agregarFila=true;

	$("#tablaTratamiento").find("tr").each(function(index, elemento) 
    {
    	var dienteAsignado;

    	if(!agregarFila)
    	{
    		return false;
    	}

        $(elemento).find("td").each(function(index2, elemento2)
        {
        	if(index2==0)
        	{
        		dienteAsignado=$(elemento2).text();
        	}
        	switch(index2)
        	{
        		case 2:
        			var partesEstado=$(elemento2).text().split("-");
        			if(partesEstado[0]!="15" && partesEstado[0]!="16" && partesEstado[0]!="17" && partesEstado[0]!="18")
        			{
        				if((partesEstado[1]==estado.split("-")[1]) && dienteAsignado==diente)
        				{
        					alert("El tratamiento ya fue asignado");
        					agregarFila=false;
        				}
        			}
        			break;
        	}
        });
    });

	if(agregarFila)
	{
		var num = $("#tablaTratamiento > tbody > tr").length;
		num = num + 1;
		var posElement = estado.indexOf(" ($");
		// alert(posElement);
		var newEstado = estado.substring(0, posElement);
		var newPrecio = estado.substring(posElement+2, estado.indexOf(")"));
		// alert(newEstado);
		// alert(newPrecio);
		var filaHtml="<tr><td><input type='hidden' class='diente"+num+"' name='dientes[]' value='"+diente+"'>"+diente+"</td><td><input type='hidden' class='cara"+num+"' name='caras[]' value='"+cara+"'>"+cara+"<td><input type='hidden' class='estado"+num+"' name='enfermedades[]' value='"+newEstado+"'>"+newEstado+"</td><td><input type='hidden' class='precio"+num+"' name='precios[]' value='"+newPrecio+"'>"+newPrecio+"</td></td><td class='widthEditarTable'><input type='button' class='button2' value='Eliminar' onclick='quitarTratamiento(this);'></td></tr>";
		$("#tablaTratamiento > tbody").append(filaHtml);
		$("#divTratamiento").scrollTop($("#tablaTratamiento").height());
		
		var precios = [];
		var numeroChild = $("#tablaTratamiento > tbody")[0]['children'];
		for (var i = 0; i < numeroChild.length; i++){
			var numeroChildChil = numeroChild[i]['children'];
			for (var j = 0; j < numeroChildChil.length; j++) {
				var numeroChildChildChild = numeroChildChil[j]['children'][0];
				if(j==3){
					var className = numeroChildChildChild['classList'][0];
					precios.push($("."+className).val());
				}
			}
		}
		var totalPrecio = 0;
		for (var i = 0; i < precios.length; i++) {
			var prec = precios[i];
			var nprec = prec.substring(1);
			var nPosPrec = nprec.indexOf(",");
			var prec1 = nprec.substring(0, nPosPrec);
			var prec2 = nprec.substring(nPosPrec+1);
			var precf = prec1+"."+prec2;
			// var precioFinal = parseFloat(precf);
			totalPrecio += parseFloat(precf);
		}
		var tPrecio = totalPrecio.toString();
		var nPosPrec = tPrecio.indexOf(".");
		if(nPosPrec!= -1){
			var prec1 = tPrecio.substring(0, nPosPrec);
			var prec2 = tPrecio.substring(nPosPrec+1);
			var precf = prec1+","+prec2;
			precf = precf+"0";
		}else{
			var precf = tPrecio+",00";
		}
		// console.log(totalPrecio);
		// console.log(tPrecio);
		// console.log(precf);
		$("#presupuestoValor").html(precf);
	}
}

function quitarTratamiento(elemento)
{
	$(elemento).parent().parent().remove();
	var precios = [];
		var numeroChild = $("#tablaTratamiento > tbody")[0]['children'];
		for (var i = 0; i < numeroChild.length; i++){
			var numeroChildChil = numeroChild[i]['children'];
			for (var j = 0; j < numeroChildChil.length; j++) {
				var numeroChildChildChild = numeroChildChil[j]['children'][0];
				if(j==3){
					var className = numeroChildChildChild['classList'][0];
					precios.push($("."+className).val());
				}
			}
		}
		var totalPrecio = 0;
		for (var i = 0; i < precios.length; i++) {
			var prec = precios[i];
			var nprec = prec.substring(1);
			var nPosPrec = nprec.indexOf(",");
			var prec1 = nprec.substring(0, nPosPrec);
			var prec2 = nprec.substring(nPosPrec+1);
			var precf = prec1+"."+prec2;
			// var precioFinal = parseFloat(precf);
			totalPrecio += parseFloat(precf);
		}
		var tPrecio = totalPrecio.toString();
		var nPosPrec = tPrecio.indexOf(".");
		if(nPosPrec!= -1){
			var prec1 = tPrecio.substring(0, nPosPrec);
			var prec2 = tPrecio.substring(nPosPrec+1);
			var precf = prec1+","+prec2;
			precf = precf+"0";
		}else{
			var precf = tPrecio+",00";
		}
		// console.log(totalPrecio);
		// console.log(tPrecio);
		// console.log(precf);
		$("#presupuestoValor").html(precf);
}

function recuperarDatosTratamiento(callback)
{
	var codigoPaciente;
	var codigoProfesional;
	var estados="";
	var descripcion;

	codigoPaciente=$("#txtCodigoPaciente").val();
	codigoProfesional=$("#txtCodigoProfesional").val();

	$("#tablaTratamiento").find("tr").each(function(index, elemento) 
    {
        $(elemento).find("td").each(function(index2, elemento2)
        {
        	estados+=$(elemento2).text()+"_";
        });
    });

    descripcion=$("#txtDescripcion").val();
    estados=estados.substring(0, estados.length-2);

    callback(codigoPaciente, codigoProfesional, estados, descripcion);
}

function guardarTratamiento()
{
	recuperarDatosTratamiento(function(codigoPaciente, codigoProfesional, estados, descripcion)
	{
		if(estados=="")
		{
			alert("Ud. debe agregar algún Tratamiento");
			return;
		}
		$.post("registrar.php",
	    {
	    	codigoPaciente: codigoPaciente,
	    	codigoProfesional: codigoProfesional,
	    	estados: estados,
	    	descripcion: descripcion
	    }, 
	    function(pagina)
	    {
	    	limpiarDatosTratamiento();
	    	$("#seccionPaginaAjax").html(pagina);
	    	setTimeout(function()
	    	{
	    		$("#seccionPaginaAjax").html("");
	    	}, 7000);
	    }).done(function(){
	    	cargarTratamientos('seccionTablaTratamientos', 'verodontograma.php', codigoPaciente); 
	    	cargarDientes('seccionDientes', 'dientes.php', '', codigoPaciente);
	    });

	});
}

function limpiarDatosTratamiento()
{
	$("#txtIdentificadorDienteGeneral").val("DXX");
	$("#txtDienteTratado").val("");
	$("#txtCaraTratada").val("");
	$("#txtDescripcion").val("");
	$("#tablaTratamiento").find("tr").each(function(index, row)
	{
		$(row).remove();
	});
}

function cargarDientes(idSeccion, url, estados, codigoPaciente)
{
	$.ajax(
    {
        url: url,
        type: "POST",
        data: {codigoPaciente: codigoPaciente, estados: estados},
        cache: true
    }).done(function(pagina) 
    {
        $("#"+idSeccion).html(pagina);
    });
}

function cargarTratamientos(idSeccion, url, codigoPaciente)
{
	$.ajax(
    {
        url: url,
        type: "POST",
        data: {codigoPaciente: codigoPaciente},
        cache: true
    }).done(function(pagina) 
    {
        $("#"+idSeccion).html(pagina);
    });
}

function prepararImpresion()
{
	$("body #seccionTablaTratamientos").css({"display": "none"});
	$("body #seccionRegistrarTratamiento").css({"display": "none"});
}

function terminarImpresion()
{
	$("body #seccionTablaTratamientos").css({"display": "inline-block"});
	$("body #seccionRegistrarTratamiento").css({"display": "inline-block"});
}