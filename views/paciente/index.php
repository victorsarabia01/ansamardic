
                
<link rel="stylesheet" href="resources/main.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<!-- BOTON QUE ACTIVA MODAL -->.
<div class="col-md-8">
    <?php if($this->accesoRegistrar){ ?>
        <!--<button type="button" id="abrirModal" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#trackerModal">
            Registrar
        </button>-->
        <button type="button" id="abrirModal" class="btn btn-outline-success">
      Registrar
  </button>
    <?php } ?>
</div>
<p></p>
<!-- FIN QUE ACTIVA MODAL -->
<!-- BOTON BUSCAR -->
    <!--<div class="col-md-8">
        <?php if($this->accesoConsultar){ ?>
            <div class="input-group">
                <input type="search" onKeyUp="buscar();keepNumOrDecimal(this)" name="busqueda" id="busqueda" class="form-control rounded" autocomplete="off" placeholder="Cédula del paciente" aria-label="Search" aria-describedby="search-addon" maxlength="8" />
                <button type="button" class="btn btn-outline-info">
                <i class="bi bi-search"></i>
                </button>
            </div>
        <?php } ?>
    </div>-->
<p></p>


<!-- MODAL -->
<div class="modal fade" id="trackerModal" tabindex="-1" aria-labelledby="nuevoProyecto" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
  <div class="modal-dialog" style="min-width: 75%;">
    <!--Con el min-width manejo el ancho del modal -->
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <div class="alert alert-success" role="alert">
                            <h3>Registrar paciente</h3>
                       
        </div>
        <div class="container-fluid">
          <form class="form-horizontal" id="formulario" method="post" action="">
            <input type="hidden" id="urlActual" value="index.php?c=paciente&a=guardar">
            <!-- COLUMNA -->
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nombre"><b>Cédula:</b></label>
                <input type="text" class="form-control" name="cedula" id="cedula" onKeyUp= buscarReg();keepNumOrDecimal(this) value="" aria-describedby="emailHelp" placeholder="Cedula Ejem. 22186490" maxlength="9" minlength="7" required>
                <input type="hidden" id="inputVerificarReg" name="inputVerificarReg" value="cedula">
                <div id="verificarRegistro"></div>
              </div>
              <div class="form-group col-md-6">
                <label for="descripcion"><b>Correo:</b></label>
                <input type="email" class="form-control" name="email" id="email" value="" aria-describedby="emailHelp" placeholder="example@gmail.com" maxlength="50" required>
              </div>
            </div>
            <!-- COLUMNA -->
            <div class="row">
              <div class="form-group col-md-6">
                <label for="observaciones"><b>Nombres:</b></label>
                <input type="text" id="nombres" name="nombres" class="form-control mayusculas buscar" id="nombres" value="" aria-describedby="emailHelp" placeholder="Nombres" maxlength="25" required>
              </div>
              <div class="form-group col-md-6">
                <label for="descripcion"><b>Teléfono:</b></label>
                <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="" aria-describedby="emailHelp" placeholder="04245208619" maxlength="11" required>
              </div>
            </div>
            <!-- COLUMNA -->
            <div class="row">
              <div class="form-group col-md-6">
                <label for="observaciones"><b>Apellidos:</b></label>
                <input type="text" name="apellidos" id="apellidos" class="form-control mayusculas buscar" id="nombres" value="" aria-describedby="emailHelp" placeholder="Apellidos" maxlength="25" required>
              </div>
              <div class="form-group col-md-3">
                <label for="fecha_ini"><b>Fecha de nacimiento:</b></label>
                <input type="date" class="form-control" id="fecha" name="fecha" required>
              </div>
              <div class="form-group col-md-3">
                <label for="fecha_ini"><b>Género:</b></label>
                <select  name="sexo" id="sexo" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="x">Selecciona</option>
                                <option value="m">M</option>
                                <option value="f">F</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="fecha_ini"><b>Dirección:</b></label>
                <textarea id="direccion" name="direccion" class="form-control" id="exampleFormControlTextarea1" value="" rows="3" placeholder="Direccion" maxlength="100" required></textarea>
          
              </div>

              
            </div>
           
          
        </div>
      </div>
      <!--.modal-body-->
      <div class="modal-footer">
        <button type="submit" id="btnguardar" class="btn btn-outline-success">Registrar</button>
       
        <button type="button" class="btn btn-sebtn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>
     </form> 
    </div>
    <!--.modal-content-->
  </div>
  <!--.modal-dialog-->
</div>

<!-- FIN MODAL -->

<div id="refrescar">
  <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Cédula</th>
                <th>Nombres   Apellidos</th>
                <th>Teléfono</th>
                <th>Email</th>
                <th>Estado</th>
                <th>EDITAR</th>
                <th>AGENDAR CITA</th>
                <th>CONDICION MÉDICA</th>
                <th>ELIMINAR</th>
            </tr>
        </thead>
        <tbody>
          <?php foreach ($this->mode->Consultar("listarPaciente") as $k) : ?>
          <tr>
                    <th scope="row"><?php echo $k->cedula; ?> </th>
              
                    <td><?php echo $k->nombres; ?></td>
                    <td><?php echo $k->apellidos; ?></td>
                    <td><?php echo $k->email; ?></td>
                    <td><?php echo $k->status; ?></td>
                  
                      <td>
                        <a href="index.php?c=paciente&a=editar&id=<?php echo $k->id; ?>" type="button" class="btn btn-outline-primary">
                          <i class="bi bi-pencil-square"></i>
                        </a>
                      </td>
                      
                      <td>
                        <a href="index.php?c=paciente&a=agendarCita&id=<?php echo $k->id; ?>" type="button" class="btn btn-outline-info">
                          <i class="bi bi-calendar-check"></i>
                        </a>
                      </td>
                      <td>
                        <a href="index.php?c=paciente&a=asignarCondicion&id=<?php echo $k->id; ?>" type="button" class="btn btn-outline-info">
                          <i class="bi bi-bag-heart"></i>
                        </a>
                      </td>
                      
                      <td>
                        <button href="#" id="<?php echo $k->id; ?>" type="button" class="btn btn-outline-danger eliminar">
                          <i class="bi bi-trash"></i>
                        </button>
                      </td>
                
          </tr>
            <?php endforeach; ?>
        </tbody>
          
    </table>
</div>


<div class="row" style="overflow-x: scroll;">
    <div id="resultadoBusqueda"></div>
    <input type="hidden" id="controlador" name="controlador" value="paciente">
    <!--<div id="tabla"></div>-->
</div>


 <script type="text/javascript">
            $(document).ready(function(){
              $(".eliminar").click(function(e){
                console.log();
                e.preventDefault();
                var id = $(this).attr("id");
                swal({
                  title: "Atención!!!",
                  text: "¿Esta seguro de eliminar el registro?!",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Confirmar",
                  cancelButtonText: "Cancelar",
                  closeOnConfirm: false,
                  closeOnCancel: false
                }, function(isConfirm) {
                  if (isConfirm) {
                    //Si SE confirma la eliminacion se ejecuta el reenvio al php encargado
                    //window.location.href="index.php?c=paciente&a=inhabilitar&id="+id;

                    $.ajax({
                          type:"POST",
                          url:"index.php?c=paciente&a=inhabilitar&id="+id,
                    
                          success:function(r){
                            if(r==1){
                     
                              swal("Atención!", "Registro Eliminado", "warning")
                             
              
                              /*$("#refrescar").load(" #refrescar");
                              var table = $('#example').DataTable( {
                                  ajax: "data.json"
                              } );
                              setInterval( function () {
                              table.ajax.reload( null, false ); // user paging is not reset on reload
                              }, 30000 );*/
                              
                            }else {
                              swal("Atención!", "Error al eliminar", "error")
                            }
                          }

                     });


                  } else {
                    //Si se cancela se emite un mensaje
                    swal("Cancelado", "Usted ha cancelado la acción de eliminación", "error");
                  }
                });
              });
            });
          </script>

           



    <script type="text/javascript">
      $(document).ready(function() {
      //let table = new DataTable('#example');
      $('#example').DataTable({

    language: {
        url: '//cdn.datatables.net/plug-ins/1.13.7/i18n/es-MX.json',
    },
    dom: 'Bfrtilp',
  buttons: [
    {
      extend: 'excelHtml5',
      text: '<i class="fas fa-file-excel"></i> ',
      titleAttr: 'Exportar a Excel',
      className: 'btn btn-success',
    },
    {
      extend: 'pdfHtml5',
      text: '<i class="fas fa-file-pdf"></i> ',
      titleAttr: 'Exportar a PDF',
      className: 'btn btn-danger',
    },
    {
      extend: 'print',
      text: '<i class="fa fa-print"></i> ',
      titleAttr: 'Imprimir',
      className: 'btn btn-info',
    },
  ],

      });
     
      });
      
    </script>
    <!--<script type="text/javascript" src="resources/datatables.js"></script>-->
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
<!--<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>-->
<script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.print.min.js"></script>

<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL PACIENTE BUSCADO -->


<!--<script>
$(document).ready(function() {
    $("#resultadoBusqueda").html('');
    //console.log('hola');
});


function buscar() {
    var textoBusqueda = $("input#busqueda").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=paciente&a=buscarRegistro", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#resultadoBusqueda").html('');
        };
};
</script>-->
<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL PACIENTE -->

<!--<script>
$(document).ready(function() {
    $("#verificarRegistroPaciente").html('');
});

function buscarPaciente() {
    var textoBusqueda = $("input#cedula").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=paciente&a=verificarRegistroPaciente", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#verificarRegistroPaciente").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#verificarRegistroPaciente").html('');
        };
};
</script>-->


<!-- FUNCICON JS PARA RECARGAR LOS REGISTROS SI EXISTE EL PACIENTE -->

<!--<script>
setInterval( function(){
    $('#tablaPaciente').load('index.php?c=paciente&a=cargarTablaPacientes');
},3000)
</script>-->


