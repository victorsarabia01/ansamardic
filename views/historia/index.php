<style type="text/css">
  #permanente{
    display:none;
   
  }
  #decidua{
    display:none;
  }
  #mixta{
    display:none;
  }
</style>

<div class="container py-3">
    <h2 class="text.center">CONSULTA HISTORIA CLINICA</h2>

    <div class="row justif">
      <div class="col-md-8">
        <?php if($this->accesoRegistrar){ ?>
          <a  class="btn btn-outline-success" id="openModal" href="#" data-bs-toggle="modal" data-bs-target="#nuevoModal">
            <i class="align-middle me-2" data-feather="plus-circle"></i>
            NUEVO REGISTRO
          </a>
        <?php } ?>
      </div>

        <?php if($this->accesoConsultar){ ?>
          <form action="">
            <div class="form-outline">
              <input  type="text" id="campo" name="campo" class="form-control" placeholder="Type query" aria-label="Search" />
            </div>
          </form>
        <?php } ?>
    </div>

    <table class="table table-sm table-striped table-hover mt-4">
      <thead class="table-secondary"  >
        <tr>
          <th>#</th>
          <th>cedula</th>
          <th>nombre</th>
          <th>apellido</th>
          <th>Fecha de Consulta</th>
          <th>Condición medica</th>
          <!-- <th>toma algun medicamento</th> -->
          <th>Presupuesto</th>
          <th>Precio de Atención</th>
          <?php if($this->accesoRegistrar || $this->accesoModificar){ ?>
          <th>accion</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($this->mode->Consultar("listarhistoria") as $k){ ?>
          <?php 
            $condiciones = $this->mode->Consultar("listarCondicionMedicaPaciente", $k->cedula);
            $tratamientos = $this->mode->Consultar("listarHistoriaServicios", $k->id, $k->cedula);
            $enfermedades = $this->mode->Consultar("listarEnfermedadesCita", $k->id);
          ?>
          <tr class="table">
            <td></td>
            <td><?php echo $k->cedula; ?></td>
            <td><?php echo $k->nombres; ?></td>
            <td><?php echo $k->apellidos; ?></td>
            <td><?php echo $k->fechaCita; ?></td>
            <td>
              <?php 
                foreach ($condiciones as $cond) {
                  echo " - ".$cond->descripcion."<br>";
                }
              ?>
            </td>
            <!-- <td> -->
              <?php 
                // foreach ($condiciones as $cond) {
                //   echo $cond->observacion."<br>";
                // }
              ?>
            <!-- </td> -->
            <td>
              <?php 
                // foreach ($condiciones as $cond) {
                //   echo " - ".$cond->observacion."<br>";
                // }
                $presupuesto = 0;
                foreach ($enfermedades as $cotizacion) {
                  $presupuesto += $cotizacion->precio_enfermedad;
                }
                echo "<b>$".number_format($presupuesto, 2, ',','.')."</b>";
              ?>
            </td>
            <td>
              <?php
                $precio = 0;
                foreach ($tratamientos as $cotizacion) {
                  $precio += $cotizacion->precio;
                }
                echo "<b>$".number_format($precio, 2, ',','.')."</b>";
              ?>
            </td>
            <td>
              <?php if($this->accesoRegistrar){ ?>
                <a href="?c=historia&cita=<?=$k->id; ?>&paciente=<?=$k->cedula; ?>" id="openTratarModal" class="btn btn-info">Tratar</a>
              <?php } ?>
              <?php if($this->accesoModificar){ ?>
              <!-- <a href="#" data-bs-toggle="modal" <?php echo $k->id; ?> data-bs-target="#editarModal"class="btn btn-success">Editar</a> -->
              <?php } ?>
            </td>
          </tr>
        <?php } ?>
        <a href="#" data-bs-toggle="modal" id="openTratarModal" data-bs-target="#tratarModal"class="btn btn-success" style="display:none">Tratar</a>
      </tbody>
    </table>
</div>
<?php include 'agregarmodal.php'; ?>
<?php include 'tratarmodal.php'; ?>
<?php //include 'segundoAgregarmodal.php'; ?>
<?php //include 'nuevomodal.php'; ?>
<?php //include 'editarmodal.php'; ?>
<script type="text/javascript">
$(document).ready(function(){
  var cita = "";
  var paciente = "";
  cita = '<?=$_GET['cita']; ?>';
  paciente = '<?=$_GET['paciente']; ?>';
  if(cita!="" && paciente!=""){
    document.getElementById("openTratarModal").click();
  }
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  var ci = "";
  ci = '<?=$_GET['ci']; ?>';
  if(ci!=""){
    document.getElementById("openModal").click();
  }
});
</script>

