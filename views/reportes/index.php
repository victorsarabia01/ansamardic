
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.0/chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.0/chart.umd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/4.4.0/helpers.min.js"></script>-->

<p></p>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="row">
<div class="col-sm-6">
  <div class="card">
                  <h2 class="card-header">Citas</h2>
                  <div class="card-body">
                    <h5 class="card-title">Generar Reporte Estadístico</h5>
                    <p class="card-text">Gráfico de reporte de citas completadas por mes</p>
                    <a href="#" class="btn btn-primary" onclick="generarReporteCitas()">Generar</a>
                  </div>
  </div>
</div>
<div class="col-sm-6">
  <div class="card">
                  <h2 class="card-header">Insumos</h2>
                  <div class="card-body">
                    <h5 class="card-title">Generar Reporte Estadístico</h5>
                    <p class="card-text">Gráfico de reporte de citas completadas por mes</p>
                    <a href="#" class="btn btn-primary" onclick="generarReporteInsumos()">Generar</a>
                  </div>
  </div>

</div>
</div>



<div>
  <canvas id="myChart"></canvas>
</div>




<script type="text/javascript">
  

 

  function generarReporteCitas(){
    $.ajax({
      url:'index.php?c=reportes&a=dataGrafico',
      type:'POST'

    }).done(function(resp){
      var mes = [];
      var cantidadCitas = [];
      var data = JSON.parse(resp);
      for(var i=0; i< data.length;i++){
        mes.push(data[i][0]);
        cantidadCitas.push(data[i][1]);
      }
      const ctx = document.getElementById('myChart');
    new Chart(ctx, {
    type: 'bar',
    data: {
      labels: mes,
      datasets: [{
        label: '#Citas por MES',
        data: cantidadCitas,
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
      //console.log(resp);
    })
  }
</script>
