<!-- MODAL -->
  
<div class="modal" style="overflow-y: scroll;" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
  <div class="modal-dialog" style="min-width: 75%;">
    <!--Con el min-width manejo el ancho del modal -->
    <div class="modal-content">
      <form class="form-horizontal" method="post" action="index.php?c=usuario&a=modificar">

        <div class="modal-header">
          <h5 class="modal-title" id="staticBackdropLabel"></h5>
          <a href="index.php?c=usuario" class="btn-close" aria-label="Close"></a>
        </div>

        <div class="modal-body">
          <div class="alert alert-success" role="alert">
            <h3>Modificar permiso</h3>
          </div>
          <div class="container-fluid">
            <div class="row">
                <input type="hidden" name="id" id="id" value="<?php echo $this->alm->id; ?>">
                <div class="form-group col-md-6">
                    <label for="empleado"><b>Empleado:</b></label>
                    <select class="form-control" name="empleado" id="empleado" required>
                        <option value=""></option>
                        <?php foreach ($this->empleado->Consultar("listarEmpleado") as $emp) { ?>
                            <option <?php if($emp->id==$this->alm->id_empleado){ echo "selected"; } ?> value="<?=$emp->id; ?>" ><?=$emp->nombres." ".$emp->apellidos." (".$emp->cedula.")"; ?></option>
                        <?php } ?>
                    </select>
                    <div id="verificarEmpleado"></div>
                </div>

                <div class="form-group col-md-6">
                    <label for="rol"><b>Rol:</b></label>
                    <select class="form-control" name="rol" id="rol" required>
                        <option value=""></option>
                        <?php foreach ($this->rol->Consultar("listarRol") as $role) { ?>
                            <option <?php if($role->id==$this->alm->id_rol){ echo "selected"; } ?> value="<?=$role->id; ?>" ><?=$role->nombre; ?></option>
                        <?php } ?>
                    </select>
                    <div id="verificarUsuario"></div>
                </div>
            </div>
            <div class="row">
              <div class="form-group col-md-4">
                <label for="usuario"><b>Nombre de Usuario:</b></label>
                <input type="text" class="form-control" name="usuario" id="usuario" value="<?php echo $this->alm->usuario; ?>" aria-describedby="emailHelp" maxlength="50" placeholder="Nombre de usuario" required>
                <div id="verificarUsuario"></div>
              </div>

              <div class="form-group col-md-4">
                <label for="password"><b>Password del usuario:</b></label>
                <input type="password" class="form-control" name="password" id="password" value="<?php echo $this->alm->password; ?>" aria-describedby="emailHelp" minlength="6" maxlength="100" placeholder="Password de usuario" required>
                <div id="verificarPassword"></div>
              </div>

              <div class="form-group col-md-4">
                <label for="passwordd"><b>Confirmar password:</b></label>
                <input type="password" class="form-control" name="passwordd" id="passwordd" value="<?php echo $this->alm->password; ?>" aria-describedby="emailHelp" minlength="6" maxlength="100" placeholder="Confirmar Password" required>
                <div id="verificarPasswordd"></div>
              </div>
            </div>
              
              <div class="form-group col-md-3">
                <label for="site"><b>Status:</b></label>           
                <select name="status" id="status" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                    <?php foreach ($this->mode->Consultar("listarStatus") as $f){ ?>
                        <option value="<?php echo $f->id ?>" <?php if($f->id == $this->alm->status){ echo 'selected'; } ?>> <?php echo $f->status; ?></option>
                    <?php } ?>
                </select>
          
              </div>
          </div>
        </div>
        <!--.modal-body-->
        <div class="modal-footer">
          <button type="submit" class="btn btn-outline-success">Guardar</button>
          <a class="btn btn-outline-danger" href="index.php?c=usuario">Cancelar</a>
        </div>
      </form> 

    </div>
    <!--.modal-content-->
  </div>
  <!--.modal-dialog-->
</div>

<!-- FIN MODAL -->
<script>
$(document).ready(function(){
  exampleModal.style.display = 'block';
});

let modal1 = document.getElementById('exampleModal');
let flex1 = document.getElementById('flex1');
let abrirModificar = document.getElementById('abrirModificar');
let cerrar1 = document.getElementById('close1');

abrirModificar.addEventListener('click', function(){
    modal1.style.display = 'block';
});

cerrar1.addEventListener('click', function(){
    modal1.style.display = 'none';
});

window.addEventListener('click', function(e){
    console.log(e.target);
    if(e.target == flex1){
        modal1.style.display = 'none';
    }
});

</script>


<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL PACIENTE -->

<script>
$(document).ready(function() {
    $("#verificarRegistroPaciente").html('');
});

// function buscarPaciente() {
//     var textoBusqueda = $("input#cedula").val();
    
//      if (textoBusqueda != "") {
//         $.post("index.php?c=paciente&a=verificarRegistroPaciente", {valorBusqueda: textoBusqueda}, function(mensaje) {
//             $("#verificarRegistroPaciente").html(mensaje);
//             //$("#idProducto").html(mensaje1);
//             //html(mennsaje1);
//          }); 
//      } else { 
//         $("#verificarRegistroPaciente").html('');
//         };
// };
</script>


<script>
$(document).ready(function() {
  var lista=[];
  var elementos=[];
    $('#asignar').click(function(){
          console.log('hizo click');
          var text="";
          var text1="";
          var condicion =  $('select[name="condicion"] option:selected').text();
          var indice = $('#condicion').val();
          
          lista.push(condicion);
          elementos.push(indice);
           document.getElementById("elementos1").innerHTML = elementos;
          /*for (var i = 0; i < lista.length; i++) {
            text += '<li>'+lista[i].condicion+'</li>';
          }
          
          document.getElementById("ulListado").innerHTML = text;*/ 
          elementos.forEach(function(condicion, index){
            text1 += `<li><input type="text" name="item[]">${index} : ${condicion}</li>`;
          //text1 += `<li><input type="text" name="item[]">${index} : ${condicion}</li>`;
          //$("#lista").html(`<li>${condicion}</li>`);
          console.log(`${indice}`);
          });  
          document.getElementById("elementos").innerHTML = text1;
          
          lista.forEach(function(condicion, index){
          text += `<li>${condicion}</li>`;
          //$("#lista").html(`<li>${condicion}</li>`);
          //console.log(`${index} : ${condicion}`);
          });  
          document.getElementById("ulListado").innerHTML = text;
          });
    $('#quitar').click(function(){
          
          var text="";
          //var condicion =  $('select[name="condicion"] option:selected').text();
          
          lista.pop();
          elementos.pop();

          lista.forEach(function(condicion, index){
          text += `<li>${condicion}</li>`;
          //$("#lista").html(`<li>${condicion}</li>`);
          //console.log(`${index} : ${condicion}`);
          });  
          document.getElementById("ulListado").innerHTML = text;
          });

           
});
</script>
