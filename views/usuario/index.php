
<!-- BOTON QUE ACTIVA MODAL -->.
<div class="col-md-8">
    <?php if($this->accesoRegistrar){ ?>
        <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#trackerModal">
            Registrar
        </button>
    <?php } ?>
</div>
<p></p>
<!-- FIN QUE ACTIVA MODAL -->
<!-- BOTON BUSCAR -->
    <div class="col-md-8">
        <?php if($this->accesoConsultar){ ?>
            <div class="input-group">
                <input type="search" onKeyUp="buscar();" name="busqueda" id="busqueda" class="form-control rounded" autocomplete="off" placeholder="Nombre del usuario" aria-label="Search" aria-describedby="search-addon" maxlength="50" />
                <button type="button" class="btn btn-outline-info">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
                    </svg>
                </button>
            </div>
        <?php } ?>
    </div>
<p></p>

<!-- FIN DEL BOTON BUSCAR -->

<!-- MODAL -->
<div class="modal fade" id="trackerModal" tabindex="-1" aria-labelledby="nuevoProyecto" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
  <div class="modal-dialog" style="min-width: 75%;">
    <!--Con el min-width manejo el ancho del modal -->
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

    <form class="form-horizontal" method="post" action="index.php?c=usuario&a=guardar">
      <div class="modal-body">
        <div class="alert alert-success" role="alert">
            <h3>Registrar usuario</h3>
        </div>
        <div class="container-fluid">
            <!-- COLUMNA -->
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="empleado"><b>Empleado:</b></label>
                    <select class="form-control" name="empleado" id="empleado" required>
                        <option value=""></option>
                        <?php foreach ($this->empleado->Consultar("listarEmpleado") as $emp) { ?>
                            <option value="<?=$emp->id; ?>" ><?=$emp->nombres." ".$emp->apellidos." (".$emp->cedula.")"; ?></option>
                        <?php } ?>
                    </select>
                    <div id="verificarEmpleado"></div>
                </div>

                <div class="form-group col-md-6">
                    <label for="rol"><b>Rol:</b></label>
                    <select class="form-control" name="rol" id="rol" required>
                        <option value=""></option>
                        <?php foreach ($this->rol->Consultar("listarRol") as $role) { ?>
                            <option value="<?=$role->id; ?>" ><?=$role->nombre; ?></option>
                        <?php } ?>
                    </select>
                    <div id="verificarUsuario"></div>
                </div>
            </div>
            <div class="row">
              <div class="form-group col-md-4">
                <label for="usuario"><b>Nombre de Usuario:</b></label>
                <input type="text" class="form-control" name="usuario" id="usuario" value="" aria-describedby="emailHelp" maxlength="50" placeholder="Nombre de usuario" required>
                <div id="verificarUsuario"></div>
              </div>

              <div class="form-group col-md-4">
                <label for="password"><b>Password del usuario:</b></label>
                <input type="password" class="form-control" name="password" id="password" value="" aria-describedby="emailHelp" minlength="6" maxlength="100" placeholder="Password de usuario" required>
                <div id="verificarPassword"></div>
              </div>

              <div class="form-group col-md-4">
                <label for="passwordd"><b>Confirmar password:</b></label>
                <input type="password" class="form-control" name="passwordd" id="passwordd" value="" aria-describedby="emailHelp" minlength="6" maxlength="100" placeholder="Confirmar Password" required>
                <div id="verificarPasswordd"></div>
              </div>
            </div>
        </div>
      </div>
      <!--.modal-body-->
      <div class="modal-footer">
        <button type="submit" class="btn btn-outline-success">Registrar</button>
       
        <button type="button" class="btn btn-sebtn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>
     </form> 
    </div>
    <!--.modal-content-->
  </div>
  <!--.modal-dialog-->
</div>

<!-- FIN MODAL -->

<div class="row" style="overflow-x: scroll;">
    <div id="resultadoBusqueda"></div>
    <div id="tablaUsuarios"></div>
</div>


<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL PACIENTE BUSCADO -->
<script>
$(document).ready(function() {
    $("#resultadoBusqueda").html('');
});

function buscar() {
    var textoBusqueda = $("input#busqueda").val();

     if (textoBusqueda != "") {
        $.post("index.php?c=usuario&a=buscarRegistro", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#resultadoBusqueda").html('');
        };
}
</script>
<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL PACIENTE -->

<script>
$(document).ready(function() {
    $("#verificarRegistroPaciente").html('');
});

// function buscarModulo() {
//     var textoBusqueda = $("input#cedula").val();
    
//      if (textoBusqueda != "") {
//         $.post("index.php?c=usuario&a=verificarRegistroPaciente", {valorBusqueda: textoBusqueda}, function(mensaje) {
//             $("#verificarRegistroPaciente").html(mensaje);
//             //$("#idProducto").html(mensaje1);
//             //html(mennsaje1);
//          }); 
//      } else { 
//         $("#verificarRegistroPaciente").html('');
//         };
// };
</script>


<!-- FUNCICON JS PARA RECARGAR LOS REGISTROS SI EXISTE EL PACIENTE -->

<script>
setInterval( function(){
    $('#tablaUsuarios').load('index.php?c=usuario&a=cargarTablaUsuarios');
},3000)
</script>


