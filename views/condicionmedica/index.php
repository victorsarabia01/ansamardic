
<!-- BOTON QUE ACTIVA MODAL -->.
<div class="col-md-8">
    <?php if($this->accesoRegistrar){ ?>
        <button type="button" id="abrirModal" class="btn btn-outline-success">
            Registrar
        </button>
    <?php } ?>
</div>
<p></p>
<!-- FIN QUE ACTIVA MODAL -->



<!-- BOTON BUSCAR -->
    <div class="col-md-8">
        <?php if($this->accesoConsultar){ ?>
            <div class="input-group">
                <input type="search" onKeyUp="buscar();" name="busqueda" id="busqueda" class="form-control rounded mayusculas buscar"  autocomplete="off" placeholder="Condición médica" aria-label="Search" aria-describedby="search-addon" maxlength="50" />
                <button type="button" class="btn btn-outline-info">
                <i class="bi bi-search"></i>   
                </button>
            </div>
        <?php } ?>
    </div>
<p></p>
<!-- MODAL PARA REGISTRAR -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
<div class="card-body">

                        <div class="alert alert-success" role="alert">
                            <h3>Condición médica</h3>
                       
                        </div>
                       
                    <form class="form-horizontal" id="formulario" method="post" action="">
                    <input type="hidden" id="urlActual" name="urlActual" value="index.php?c=condicionmedica&a=guardar">
                        <div class="col-md-40">
                        
                       
                        </div>
                        <p></p>
                        <div class="col-md-40">
                            </label><b>Nombre de la condición médica:</b></label>
                          
                            <input type="text area" id="descripcion" name="descripcion" onkeypress="return permite(event, 'num_car')" onKeyUp="buscarReg();" class="form-control mayusculas buscar" value="" aria-describedby="emailHelp" placeholder="Nombre" maxlength="50" required>
                            <input type="hidden" name="inputVerificarReg" id="inputVerificarReg" value="descripcion">
              
                        </div>
                        <div id="verificarRegistro"></div>
                        <p></p>
                        <div class="form-group col-md-40">
                            <label for="fecha_ini"><b>Observaciones:</b></label>
                         
                            <textarea id="observacion" name="observacion" class="form-control" id="exampleFormControlTextarea1"  rows="8" placeholder="Observaciones" maxlength="250" required></textarea>
          
                         </div>
</div>
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="submit" href="#" name="btnguardar" id="btnguardar" class="btn btn-outline-success">Registrar</button>
        <button type="button" class="btn btn-sebtn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- FIN DEL MODAL -->
<div id="resultadoBusqueda"></div>
<input type="hidden" name="controlador" id="controlador" value="condicionMedica">
<div id="tabla"></div>



<!-- VALIDACIONES FRONT -->
<!--<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
     // Reemplace todos los no numéricos primero, excepto la suma numérica.
    obj.value = obj.value.replace(/[^\d.]/g,"");
     // Debe asegurarse de que el primero sea un número y no.
    obj.value = obj.value.replace(/^\./g,"");
     // Garantizar que solo hay uno. No más.
    obj.value = obj.value.replace(/\.{2,}/g,".");
     // Garantía. Solo aparece una vez, no más de dos veces
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

    <script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z ]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>-->



<!-- FUNCICON JS PARA BUSCAR REGISTROS -->

<!--<script>
$(document).ready(function() {
    $("#resultadoBusqueda").html('');
});

function buscar() {
    var textoBusqueda = $("input#busqueda").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=condicionmedica&a=buscarRegistro", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#resultadoBusqueda").html('');
        };
};
</script>-->


<!--Función de cargar la tabla principal del Condicion Medica-->
<!--<script>
setInterval( function(){

$('#resultadoBusquedaCondicionMedica').load('index.php?c=condicionmedica&a=cargarCondicionMedica');

},3000)
</script>-->

<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE LA CONDICON MEDICA -->


<!--<script>
  $(document).ready(function() {
    $("#verificarRegistroCondicionMedica").html('');
});

function buscarCondicionMedica() {
    var textoBusqueda = $("input#descripcion").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=condicionmedica&a=verificarElRegistroCondicionMedica", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#verificarRegistroCondicionMedica").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#verificarRegistroCondicionMedica").html('');
        };
};
</script>-->
    
