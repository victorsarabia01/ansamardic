

<!-- BOTON QUE ACTIVA MODAL -->.
<!--<div class="col-md-8">
  <button type="button" class="btn btn-outline-success" data-bs-toggle="modal" data-bs-target="#staticBackdrop" onclick="abrirModalx()">
  Registrar
  </button>
</div>-->
<div class="col-md-8">
  <?php if($this->accesoRegistrar){ ?>
  <button type="button" class="btn btn-outline-success" onclick="abrirModal()">
  Registrar
  </button>
  <?php } ?>
</div>
<p></p>
<!-- FIN QUE ACTIVA MODAL -->



<!-- BOTON BUSCAR -->
                  <div class="col-md-8">
                    <?php if($this->accesoConsultar){ ?>
                        <div class="input-group">
                            <input type="search" onkeypress="return permite(event, 'num_car')" onKeyUp="buscar();" name="busqueda" id="busqueda" class="form-control rounded"  autocomplete="off" placeholder="Nombre del consultorio" aria-label="Search" aria-describedby="search-addon" maxlength="50" />
                            <!--<button type="button" class="btn btn-outline-primary">search</button>-->
                        
                          <button type="button" class="btn btn-outline-info">
                          <i class="bi bi-search"></i>
                          </button>
                        </div>
                         <?php } ?>
                </div>
<p></p>
<!-- MODAL PARA REGISTRAR -->
<div class="modal fade" style="overflow-y: scroll;" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
<div class="card-body">

                        <div class="alert alert-success" role="alert">
                            <h3>Registrar consultorio</h3>
                       
                        </div>
                       
                    <form class="form-horizontal" method="post" id="formulario" name="for" action="">
                        <div class="col-md-40">
                        <input type="hidden" name="id" id="id" value="">
                       
                        </div>
                        
                        <div class="col-md-40">
                            </label><b>Nombre:</b></label>
                            <input type="text area" id="descripcion" name="descripcion" onkeypress="return permite(event, 'num_car')" onKeyUp="buscarConsultorio();" class="form-control" value="" aria-describedby="emailHelp" placeholder="Ejemplo. Dental la 48" maxlength="50" required>
                      
              
                        </div>
                        <div id="verificarRegistroConsultorio"></div>
            
                        <div class="col-md-40">
                        </label><b>Dirección:</b></label>
                        <textarea id="direccion" name="direccion" class="form-control" id="exampleFormControlTextarea1" value="" rows="4" placeholder="Ejemplo. Carrera 13A entre 3 y 5 San Francisco" maxlength="100" required></textarea>
                        </div>
          
                        <div class="col-md-40">
                        </label><b>Cantidad de sillón dental:</b></label>
                       
                        <select class="form-select form-select-lg mb-1" name="sillas" id="sillas" class="form-select form-select-lg mb-1" required>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                        </div>
              
            <div class="row">
 
              <div class="form-group col-md-6">
                <label for="nombre"><b>Número teléfono:</b></label>
                <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="" aria-describedby="emailHelp" placeholder="04245208619" maxlength="11" required>
              </div>
            </div>                      
                            
                            
                     
                        
            
</div>
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="submit" href="?c=guardarConsultorio" value="Guardar" name="btnguardar" id="btnguardar" class="btn btn-outline-success">Registrar</button>
        <button type="button" class="btn btn-sebtn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- FIN DEL MODAL -->
<div class="row" style="overflow-x: scroll;">
<div id="resultadoBusqueda"></div>
<div id="tablaConsultorio"></div>
</div>
<!-- COMIENZO DE TABLA QUE MUESTRA LOS REGISTROS -->

<!-- FUNCICON JS PARA BUSCAR REGISTROS -->

<script>
$(document).ready(function() {
    $("#resultadoBusqueda").html('');
});

function buscar() {
    var textoBusqueda = $("input#busqueda").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=consultorio&a=buscarRegistro", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#resultadoBusqueda").html('');
        };
};
</script>
<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL CONSULTORIO -->

<script>
$(document).ready(function() {
    $("#verificarRegistroConsultorio").html('');
});

function buscarConsultorio() {
    var textoBusqueda = $("input#descripcion").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=consultorio&a=verificarRegistroConsultorio", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#verificarRegistroConsultorio").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#verificarRegistroConsultorio").html('');
        };
};
</script>


<script type="text/javascript">
  $( function() {
    $("#codTlfno").change( function() {
        if ($(this).val() == "5") {
            $("#codTlfno1").prop("disabled", false);
        } else {
            $("#codTlfno1").prop("disabled", true);
        }
    });
});
</script>

<script>
setInterval( function(){

$('#tablaConsultorio').load('index.php?c=consultorio&a=cargarTablaConsultorios');

},3000)
</script>

<script type="text/javascript">
  $(document).ready(function(){
$('#btnguardar').click(function(){
  var datos=$('#formulario').serialize();

    $.ajax({
      type:"POST",
      url:"index.php?c=consultorio&a=guardar",
      data:datos,
      success:function(r){
        if(r==1){
 
          swal("Excelente", "Registro Exitoso", "success")
          $('#staticBackdrop').modal('hide');
        }else if(r==2){

          swal("Atención!", "Error en registro", "error")

        }else {
          swal("Atención!", "Complete los campos", "error")
        }
      }

    });
    return false;
  }); 
});
</script>

<script>
function abrirModal() {
    
    $('#staticBackdrop').modal('show');
};
</script>

    
