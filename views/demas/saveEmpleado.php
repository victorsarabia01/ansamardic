<?php
	//include_once 'controller/control.php';
	include_once 'views/header.php';
?>
<!DOCTYPE html>
<html>
<head>
    
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>


    <script type="text/javascript" src="resources/js/Jquery.js"></script>
    
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  
  <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            <h3>Aqui puedes, Registrar Empleado</h3>
                       
                        </div>
                       
                     <form class="form-horizontal" method="post" action="index.php?c=nuevoEmpleado&a=guarda">
                        <div class="col-md-8">
                        <input type="hidden" name="txtID" id="txtID" value="<?php echo $alm->id; ?>">
                        <input type="hidden" name="cedulaAmigo" id="cedulaAmigo" value="">

                            <input type="text" class="form-control" name="cedula" id="cedula" onKeyUp= keepNumOrDecimal(this) value="" aria-describedby="emailHelp" placeholder="Cedula Ejem. 22186490" maxlength="8" required>
                        </div>
              
                        <div class="col-md-8">
                            <input type="text" id="nombres" name="nombres" class="form-control mayusculas buscar" id="nombres" value="" aria-describedby="emailHelp" placeholder="Nombres" maxlength="25" required>
                        </div>
                         <div class="col-md-8">
                            <input type="text" name="apellidos" id="apellidos" class="form-control mayusculas buscar" id="nombres" value="" aria-describedby="emailHelp" placeholder="Apellidos" maxlength="25" required>
                        </div>
						<div class="col-md-8">
                            
                            <input type="date" class="form-control" id="fecha" name="fecha" required>
                        </div>
						
						<div class="col-md-8">
                        
                            <input type="text" class="form-control" name="email" id="email" value="" aria-describedby="emailHelp" placeholder="example@gmail.com" maxlength="50" required>
                        </div>


                        <div class="col-md-8">
                        
                            <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="" aria-describedby="emailHelp" placeholder="04245208619" maxlength="11" required>
                        </div>
						<div class="col-md-8">

                        <textarea id="direccion" name="direccion" class="form-control" id="exampleFormControlTextarea1" value="<?php echo $alm->direccion; ?>" rows="3" placeholder="Direccion" maxlength="100" required></textarea>
						</div>


                        
                        

                        <div class="col-md-8">

                                <!--<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">3</label>
                                </div>-->
                           
                           
                             <select name="empleado" id="empleado" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Empleado</option>
								<?php foreach ($this->mode->listarTodosTipoEmpleados()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->descripcion?></option>
                                <?php endforeach ?>
                     
         
                            </select>
                         
                        </div>
                        <div id="resultadoBusqueda"></div>
                        <div id="resultadoBusqueda1"></div>
                        <br>
                        <div class="col-md-8">
                            <div class="g-recaptcha" data-sitekey="6Lcc4xInAAAAAIhChEIZvj71HnTxRnwBqVgK6daJ"></div>
                        </div>


                        <br>

                  
                            <button type="submit" href="?c=guardar" value="Guardar" name="registrar" id="registrar" class="btn btn-success">Guardar</button>
                        
                            <a href="index.php?c=plantillaPrincipal" class="btn btn-block btn-danger">Cancelar</a>
                     
                        
            </form>
  </div>
</div>
</div>


<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
     // Reemplace todos los no numéricos primero, excepto la suma numérica.
    obj.value = obj.value.replace(/[^\d.]/g,"");
     // Debe asegurarse de que el primero sea un número y no.
    obj.value = obj.value.replace(/^\./g,"");
     // Garantizar que solo hay uno. No más.
    obj.value = obj.value.replace(/\.{2,}/g,".");
     // Garantía. Solo aparece una vez, no más de dos veces
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

    <script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z ]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>





    
</body>
<?php
include_once 'views/footer.php';
?>
</html>