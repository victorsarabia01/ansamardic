<?php
			include_once 'views/header.php';
			//include_once 'views/footer.php';
?>
<!DOCTYPE html>
<html>
<head>
    
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>


    <script type="text/javascript" src="resources/js/Jquery.js"></script>
    
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  
  <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            <h3>Aqui puedes, Registrar Consultorio</h3>
                       
                        </div>
                       
                    <form class="form-horizontal" method="post" action="index.php?c=nuevoConsultorio&a=guardarConsultorio">
                        <div class="col-md-8">
                        <input type="hidden" name="txtID" id="txtID" value="<?php echo $alm->id; ?>">
                        <input type="hidden" name="cedulaAmigo" id="cedulaAmigo" value="<?php echo $alm->cedula; ?>">

                            
                        </div>
              
                        <div class="col-md-8">
                            <input type="text area" id="descripcion" name="descripcion" class="form-control mayusculas buscar" value="<?php echo $alm->descripcion; ?>" aria-describedby="emailHelp" placeholder="Descripcion" maxlength="25" required>
							
						</div>
                         <div class="col-md-8">

                        <textarea id="direccion" name="direccion" class="form-control" id="exampleFormControlTextarea1" value="<?php echo $alm->direccion; ?>" rows="3" placeholder="Direccion" maxlength="100" required></textarea>
						</div>

                 
         
                        

                        <div class="col-md-8">

                        </div>

                        <br>

                  
                            <button type="submit" href="?c=guardarConsultorio" value="Guardar" name="registrar" id="registrar" class="btn btn-success">Registrar</button>
                        
                            <a href="index.php?c=plantillaPrincipal" class="btn btn-block btn-danger">Cancelar</a>
                     
                        
            </form>
  </div>
</div>
</div>




<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
     // Reemplace todos los no numéricos primero, excepto la suma numérica.
    obj.value = obj.value.replace(/[^\d.]/g,"");
     // Debe asegurarse de que el primero sea un número y no.
    obj.value = obj.value.replace(/^\./g,"");
     // Garantizar que solo hay uno. No más.
    obj.value = obj.value.replace(/\.{2,}/g,".");
     // Garantía. Solo aparece una vez, no más de dos veces
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

    <script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z ]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>





    
</body>
<?php
include_once 'views/footer.php';
?>

</html>