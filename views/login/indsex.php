<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
    <title>
        Bienes Nacionales
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link href="<?= ASSETS; ?>css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= ASSETS; ?>css/nucleo-svg.css" rel="stylesheet" />
    <!-- CSS Files -->
    <link href="<?= ASSETS; ?>css/style_login.css" rel="stylesheet" />
    <link id="pagestyle" href="<?= ASSETS; ?>css/material-dashboard.css?v=3.0.4" rel="stylesheet" />
    <!-- BOOTSTRAP -->
    <link href="<?= ASSETS; ?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= ASSETS; ?>css/style.css" rel="stylesheet" />
    <!-- JQuery -->
    <script src="<?= ASSETS; ?>jquery/dist/jquery.min.js"></script>
    <link href="<?= ASSETS; ?>datatables/media/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <!-- Sweetalert -->
    <link href="<?= ASSETS; ?>sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" />
    <script src="<?= ASSETS; ?>sweetalert2/dist/sweetalert2.min.js"></script>
    <!-- Font Awesome Icons -->
    <link href="<?= ASSETS; ?>font-awesome/css/all.min.css" rel="stylesheet" />  
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
</head>

<body>
    <!-- ============================================================== -->
    <!-- login de Incio de Sesión -->
    <!-- ============================================================== -->
    <div class="container-center">
        <div class="splash-container">
            <div class="card ">
                <div class="card-header text-center bg-primary">
                    <img class="logo-img" src="<?= ASSETS; ?>img/logoBN-blanco.png" alt="logo">
                </div>
                <div class="card-body">
                    <form action="#" method="POST" id="formularioLogin" enctype="multipart/form-data" >
                        <div class="form-group formulario__grupo" id="grupo__usuario">
                            <div class="formulario__grupo-input">
                                <input name="usuario" id="usuario" type="text" class=" form-control" maxlength="20" placeholder="Usuario">
                                <i class="formulario__icono formulario__validacion-estado fas fa-times-circle"></i>
                            </div>
                            <p class="formulario__input-error">Solo puede contener letras y números. De 4 a 20 caracteres</p>
                        </div>
                        <div class="form-group formulario__grupo" id="grupo__clave">
                            <div class="formulario__grupo-input">
                                <input name="clave" id="clave" type="password" class=" form-control" maxlength="20" placeholder="Clave">
                                <i class="formulario__icono formulario__validacion-estado fas fa-times-circle"></i>
                            </div>
                            <p class="formulario__input-error">Puede contener de 8 a 20 caracteres</p>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Ingresar</button>
                    </form>
                    <!-- <?= var_dump($_SESSION); ?>  -->
                </div>
                <div class="card-footer bg-white p-0  ">
                    <div class="card-footer-item card-footer-item-bordered p-2 text-center">
                        <a href="#" class="footer-link">Recuperar Contraseña</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    <!-- ============================================================== -->
    <!-- Fin de Inio de Sesión login  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->

       <!-- <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script> -->
     <!-- <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script> -->
</body>
 
</html>
<script src="<?= ASSETS ?>js/general.js"></script>
<script src="<?= ASSETS ?>js/login.js"></script>
