<?php 
    if (empty($_SESSION[NAME.'_cuenta']['id'])) {
    }

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
	
	<script type="text/javascript" src="resources/msjAlert/sweetalert.js"></script>
	<link rel="stylesheet" type="text/css" href="resources/msjAlert/sweetalert.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
	<!-- ODONTOGRAMA -->
	<!--<link rel="stylesheet" href="resources/css/select2.min.css">-->
	<link href="resources/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<link rel="stylesheet" href="resources/css/cssDiente.css">
	<link rel="stylesheet" href="resources/css/cssDienteGeneral.css">
	<link rel="stylesheet" href="resources/css/cssFormulario.css">
	<link rel="stylesheet" href="resources/css/cssComponentes.css">
	<link rel="stylesheet" href="resources/css/cssComponentesPersonalizados.css">
	<link rel="stylesheet" href="resources/css/cssContenido.css">
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/jsAcciones.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js" integrity="sha384-q9CRHqZndzlxGLOj+xrdLDJa9ittGte1NksRmgJKeCV9DrM7Kz868XYqsKWPpAmn" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	
	<script src="resources/comunes.js"></script>
	<script src="resources/validacionFront.js"></script>
	<script src="resources/Registroajax.js"></script>
	<script src="resources/Modificarajax.js"></script>
	<script src="resources/condicionMedica.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-bootpag/1.0.4/jquery.bootpag.min.js"></script>

    <script src="//raw.github.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
    



	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
	<!-- FONT AWESONE -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<title>Centro Médico Ansamar</title>
	
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">

	<script src="resources/js/app.js"></script>

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">

			<ul class="nav nav-pills">
			  <li class="nav-item">
			    <a class="navbar-brand" href="index.php?c=home">Ansamar</a>
			  </li>
			  <li class="nav-item dropdown">
			    <a class="nav-link dropdown-toggle link-dark" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Gestionar consultorio</a>
			    <ul class="dropdown-menu">
			    <?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Planificacion"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				
			      <li><a class="dropdown-item " href="index.php?c=planificacion">Planificación</a></li>
			    <?php } ?>
			      <li><a class="dropdown-item" href="index.php?c=consultorio">Consultorios</a></li>
			      <li><a class="dropdown-item" href="index.php?c=proveedor">Proveedor</a></li>
			      <li><a class="dropdown-item" href="index.php?c=insumo">Insumos</a></li>
			      <li><hr class="dropdown-divider"></li>
			      <li><a class="dropdown-item" href="index.php?c=empleado">Empleados</a></li>
			      <li><a class="dropdown-item" href="index.php?c=tipoempleado">Tipo de empleado</a></li>
			    </ul>
			  </li>
			  <li class="nav-item dropdown">
			    <a class="nav-link dropdown-toggle link-dark" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Gestionar citas</a>
			    <ul class="dropdown-menu">
			      <li><a class="dropdown-item" href="index.php?c=paciente">Pacientes</a></li>
			      <li><a class="dropdown-item" href="index.php?c=condicionMedica">Condicion medica</a></li>
			      <li><a class="dropdown-item" href="index.php?c=cita">Citas</a></li>
			      <li><a class="dropdown-item" href="index.php?c=serviciodental">Servicios dentales</a></li>
			      <li><hr class="dropdown-divider"></li>
			      <li><a class="dropdown-item" href="#">Enlace separado</a></li>
			    </ul>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link link-dark" href="#">Odontograma</a>
			  </li>
			  <li class="nav-item dropdown">
			     <a class="nav-link dropdown-toggle link-dark" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Reportes</a>
			      <ul class="dropdown-menu">
			      <li><a class="dropdown-item" href="index.php?c=paciente">Pacientes</a></li>
			      <li><a class="dropdown-item" href="index.php?c=cita">Citas</a></li>
			      <li><a class="dropdown-item" href="#">Algo más aquí</a></li>
			      <li><hr class="dropdown-divider"></li>
			      <li><a class="dropdown-item" href="#">Reportes</a></li>
			      <li><hr class="dropdown-divider"></li>
			      <li><a class="dropdown-item" href="#">Estadisticos</a></li>
			    </ul>
			  </li>
			  <li class="nav-item dropdown">
			     <a class="nav-link dropdown-toggle link-dark" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Gestionar seguridad</a>
			      <ul class="dropdown-menu">
			      <li><a class="dropdown-item" href="index.php?c=rol">Roles</a></li>
			      <li><a class="dropdown-item" href="index.php?c=modulo">Modulos</a></li>
			      <li><a class="dropdown-item" href="#">Algo más aquí</a></li>
			      <li><hr class="dropdown-divider"></li>
			      <li><a class="dropdown-item" href="#">Reportes</a></li>
			      <li><hr class="dropdown-divider"></li>
			      <li><a class="dropdown-item" href="#">Estadisticos</a></li>
			    </ul>
			  </li>
			 	
			 <li class="nav-item dropdown">
							<a class="nav-link">
								<p>
									<b>Consultorio:</b> (<?php print_r($_SESSION[NAME.'_consultorio']->descripcion) ?>)
								</p>
							</a>
			</li>
	
			<li class="nav-item dropdown">
							<a class="nav-link" href="index.php?c=login&a=cerrarSession">
								<i class="align-middle" data-feather="power"></i>
							</a>
			</li>
		
			</ul>
	</nav>


	



