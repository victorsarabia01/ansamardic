<?php 
    if (empty($_SESSION[NAME.'_cuenta']['id'])) {
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/icons/icon-48x48.png" />

	<link rel="canonical" href="https://demo-basic.adminkit.io/pages-blank.html" />
	<!-- PRUEBAS LINK Y SCRIPT COMUNES -->
	
	<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
	
	<script type="text/javascript" src="resources/msjAlert/sweetalert.js"></script>
	<link rel="stylesheet" type="text/css" href="resources/msjAlert/sweetalert.css">

	<!--<link rel="stylesheet" href="resources/DataTables/datatables.css" />
 
	<script src="resources/DataTables/datatables.js"></script>-->
	
	<!-- BOOSTRAP -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

	<!-- ODONTOGRAMA -->
	<link rel="stylesheet" href="resources/css/select2.min.css">
	<link href="resources/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<link rel="stylesheet" href="resources/css/cssDiente.css">
	<link rel="stylesheet" href="resources/css/cssDienteGeneral.css">
	<link rel="stylesheet" href="resources/css/cssFormulario.css">
	<link rel="stylesheet" href="resources/css/cssComponentes.css">
	<link rel="stylesheet" href="resources/css/cssComponentesPersonalizados.css">
	<link rel="stylesheet" href="resources/css/cssContenido.css">
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/jsAcciones.js"></script>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

	<!-- ODONTOGRAMA -->

	<!-- DATATABLES -->
	<!--<link href="resources/DataTables/DataTables-1.13.6/css/dataTables.bootstrap5.min.css" rel="stylesheet">
	<link href="resources/DataTables/Buttons-2.4.1/css/buttons.bootstrap5.min.css" rel="stylesheet">
 
	<script src="resources/DataTables/JSZip-3.10.1/jszip.min.js"></script>
	<script src="resources/DataTables/pdfmake-0.2.7/pdfmake.min.js"></script>
	<script src="resources/DataTables/pdfmake-0.2.7/vfs_fonts.js"></script>
	<script src="resources/DataTables/DataTables-1.13.6/js/jquery.dataTables.min.js"></script>
	<script src="resources/DataTables/DataTables-1.13.6/js/dataTables.bootstrap5.min.js"></script>
	<script src="resources/DataTables/Buttons-2.4.1/js/dataTables.buttons.min.js"></script>
	<script src="resources/DataTables/Buttons-2.4.1/js/buttons.bootstrap5.min.js"></script>
	<script src="resources/DataTables/Buttons-2.4.1/js/buttons.colVis.min.js"></script>
	<script src="resources/DataTables/Buttons-2.4.1/js/buttons.html5.min.js"></script>
	<script src="resources/DataTables/Buttons-2.4.1/js/buttons.print.min.js"></script>
	<script src="resources/DataTables/datatable.js"></script>-->


	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js" integrity="sha384-q9CRHqZndzlxGLOj+xrdLDJa9ittGte1NksRmgJKeCV9DrM7Kz868XYqsKWPpAmn" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	
	<script src="resources/comunes.js"></script>
	<script src="resources/validacionFront.js"></script>
	<script src="resources/Registroajax.js"></script>
	<script src="resources/Modificarajax.js"></script>
	<script src="resources/condicionMedica.js"></script>




	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.5/font/bootstrap-icons.css">
	<!-- FONT AWESONE -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

 
	<!-- PRUEBAS ALERTIFY -->
	<!--

	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="resources/bootstrap-table.js"></script>
	<script src="resources/pagination.js"></script>-->



	<!-- 
	<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.22.1/dist/bootstrap-table.min.css">

	<script src="https://unpkg.com/bootstrap-table@1.22.1/dist/bootstrap-table.min.js"></script>

	<script src="https://unpkg.com/bootstrap-table@1.22.1/dist/locale/bootstrap-table-zh-CN.min.js"></script>
	<script type="text/javascript" src="https://unpkg.com/paginationjs@2.6.0/dist/pagination.js"></script>
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.css" />
  
	<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"></script>


	<link href="resources/DataTables/jquery.dataTables.min.css" rel="stylesheet">	
	<script src="resources/DataTables/jquery.dataTables.min.js"></script>



<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
 
<script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>-->






	<!--<link rel="stylesheet" type="text/css" href="resources/alertifyjs/css/alertify.css">
	<link rel="stylesheet" type="text/css" href="resources/alertifyjs/css/themes/default.css">
	<script src="resources/alertifyjs/alertify.js"></script>-->

	<title>Centro Médico Ansamar</title>
	
	<link href="resources/css/app.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
</head>

<body>
<script type="text/javascript">
$(document).ready(function(){
console.clear();
});
</script>
	<div class="wrapper">
		<nav id="sidebar" class="sidebar js-sidebar">
			<div class="sidebar-content js-simplebar">
				<a class="sidebar-brand text-center" href="">
					<span class="text-center">Centro Médico Ansamar</span>
				</a>
				

				<li class="sidebar-nav">

					<a class="sidebar-link " href="index.php?c=home">
						<i class="align-middle" data-feather="sliders"></i> <span class="align-middle"><b>Inicio</b></span>
					</a>
				</li>
				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Planificacion"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link " href="index.php?c=planificacion">
						<i class="align-middle" data-feather="clock"></i> <span class="align-middle">Planificación</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Cita"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link " href="index.php?c=cita">
						<i class="align-middle" data-feather="list"></i> <span class="align-middle">Cita</span>
					</a>
				</li>
				<?php } ?>
				<!--<li class="sidebar-item">
					<a class="sidebar-link" href="index.php?c=listarCitas">
						<i class="align-middle" data-feather="list"></i> <span class="align-middle">Listar Citas</span>
					</a>
				</li>-->
				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Consultorio"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=consultorio">
						<i class="align-middle" data-feather="home"></i> <span class="align-middle">Consultorio</span>
					</a>
				</li>
				<?php } ?>


				<!--<li class="sidebar-item">
					<a class="sidebar-link" href="#">
						<i class="align-middle" data-feather="book"></i> <span class="align-middle">Condicion Medica</span>
					</a>
				</li>-->

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Paciente"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=paciente">
						<i class="align-middle" data-feather="user"></i> <span class="align-middle">Paciente</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Condicion Medica"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=condicionmedica">
						<i class="align-middle" data-feather="book"></i> <span class="align-middle">Condición médica</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Empleado"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=empleado">
						<i class="align-middle" data-feather="user"></i> <span class="align-middle">Empleado</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Tipo De Empleado"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=tipoempleado">
						<i class="align-middle" data-feather="user"></i> <span class="align-middle">Tipo de empleado</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Odontograma"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=historia">
						<i class="align-middle" data-feather="book"></i> <span class="align-middle">Historia</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Servicio Dental"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=serviciodental">
						<i class="align-middle" data-feather="star"></i> <span class="align-middle">Servicio dental</span>
					</a>
				</li>
				<?php } ?>

				<!--<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Consulta"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="#">
						<i class="align-middle" data-feather="heart"></i> <span class="align-middle">Consulta</span>
					</a>
				</li>
				<?php } ?>-->

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Insumos"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=insumo">
						<i class="align-middle" data-feather="briefcase"></i> <span class="align-middle">Insumos</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Reportes"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=reportes">
						<i class="align-middle" data-feather="file-plus"></i> <span class="align-middle">Reportes</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenu = false;
					foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Usuario"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
				?>
				<?php if($accesoModuloMenu){ ?>
				<li class="sidebar-nav">
					<a class="sidebar-link" href="index.php?c=usuario">
						<i class="align-middle" data-feather="users"></i> <span class="align-middle">Usuarios</span>
					</a>
				</li>
				<?php } ?>

				<?php 
					$accesoModuloMenuSeguridad = false;
					foreach ($this->accesos as $acc){ 
						if($acc->nombre_modulo=="Bitacora"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenuSeguridad = true; } }
						else if($acc->nombre_modulo=="Rol"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenuSeguridad = true; } }
						else if($acc->nombre_modulo=="Modulo"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenuSeguridad = true; } }
						else if($acc->nombre_modulo=="Permiso"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenuSeguridad = true; } }
					}
				?>
				<?php if($accesoModuloMenuSeguridad){ ?>
				<ul class="sidebar-nav">
					<li class="sidebar-header">
						Seguridad
					</li>
					
					<?php 
						$accesoModuloMenu = false;
						foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Bitacora"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
					?>
					<?php if($accesoModuloMenu){ ?>
					<li class="sidebar-item">
						<a class="sidebar-link" href="#">
							<i class="align-middle" data-feather="user"></i> <span class="align-middle">Bitácora</span>
						</a>
					</li>
					<?php } ?>

					<?php 
						$accesoModuloMenu = false;
						foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Rol"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
					?>
					<?php if($accesoModuloMenu){ ?>
					<li class="sidebar-item">
						<a class="sidebar-link" href="index.php?c=rol">
							<i class="align-middle" data-feather="file"></i> <span class="align-middle">Roles</span>
						</a>
					</li>
					<?php } ?>

					<?php 
						$accesoModuloMenu = false;
						foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Modulo"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
					?>
					<?php if($accesoModuloMenu){ ?>
					<li class="sidebar-item">
						<a class="sidebar-link" href="index.php?c=modulo">
							<i class="align-middle" data-feather="file"></i> <span class="align-middle">Modulos</span>
						</a>
					</li>
					<?php } ?>
					
					<?php 
						$accesoModuloMenu = false;
						foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Permiso"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
					?>
					<?php if($accesoModuloMenu){ ?>
					<li class="sidebar-item">
						<a class="sidebar-link" href="index.php?c=permiso">
							<i class="align-middle" data-feather="file"></i> <span class="align-middle">Permisos</span>
						</a>
					</li>
					<?php } ?>

					<?php 
						$accesoModuloMenu = false;
						foreach ($this->accesos as $acc){ if($acc->nombre_modulo=="Permiso"){ if($acc->nombre_permiso=="Consultar"){ $accesoModuloMenu = true; } } }
					?>
					<?php if($accesoModuloMenu){ ?>
					<li class="sidebar-item">
						<a class="sidebar-link" href="index.php?c=backup&a=backup">
							<i class="align-middle" data-feather="file"></i> <span class="align-middle">Backup</span>
						</a>
					</li>
					<?php } ?>

				</ul>
				<?php } ?>
			</div>
		</nav>

		<div class="main">
			<nav class="navbar navbar-expand navbar-light navbar-bg">
				<a class="sidebar-toggle js-sidebar-toggle">
					<i class="hamburger align-self-center"></i>
				</a>

				<div class="navbar-collapse collapse">
					<ul class="navbar-nav navbar-align">
						<li class="nav-item dropdown">
							<a class="nav-link">
								<p>
									(<?php print_r($_SESSION[NAME.'_consultorio']->descripcion) ?>)
								</p>
							</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
								<div class="position-relative">
									<i class="align-middle" data-feather="bell"></i>
									<span class="indicator">4</span>
								</div>
							</a>
							<div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0" aria-labelledby="alertsDropdown">
								<div class="dropdown-menu-header">
									4 New Notifications
								</div>
								<div class="list-group">
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<i class="text-danger" data-feather="alert-circle"></i>
											</div>
											<div class="col-10">
												<div class="text-dark">Update completed</div>
												<div class="text-muted small mt-1">Restart server 12 to complete the update.</div>
												<div class="text-muted small mt-1">30m ago</div>
											</div>
										</div>
									</a>
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<i class="text-warning" data-feather="bell"></i>
											</div>
											<div class="col-10">
												<div class="text-dark">Lorem ipsum</div>
												<div class="text-muted small mt-1">Aliquam ex eros, imperdiet vulputate hendrerit et.</div>
												<div class="text-muted small mt-1">2h ago</div>
											</div>
										</div>
									</a>
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<i class="text-primary" data-feather="home"></i>
											</div>
											<div class="col-10">
												<div class="text-dark">Login from 192.186.1.8</div>
												<div class="text-muted small mt-1">5h ago</div>
											</div>
										</div>
									</a>
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<i class="text-success" data-feather="user-plus"></i>
											</div>
											<div class="col-10">
												<div class="text-dark">New connection</div>
												<div class="text-muted small mt-1">Christina accepted your request.</div>
												<div class="text-muted small mt-1">14h ago</div>
											</div>
										</div>
									</a>
								</div>
								<div class="dropdown-menu-footer">
									<a href="#" class="text-muted">Show all notifications</a>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-icon dropdown-toggle" href="#" id="messagesDropdown" data-bs-toggle="dropdown">
								<div class="position-relative">
									<i class="align-middle" data-feather="message-square"></i>
								</div>
							</a>
							<div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0" aria-labelledby="messagesDropdown">
								<div class="dropdown-menu-header">
									<div class="position-relative">
										4 New Messages
									</div>
								</div>
								<div class="list-group">
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<img src="img/avatars/avatar-5.jpg" class="avatar img-fluid rounded-circle" alt="Vanessa Tucker">
											</div>
											<div class="col-10 ps-2">
												<div class="text-dark">Vanessa Tucker</div>
												<div class="text-muted small mt-1">Nam pretium turpis et arcu. Duis arcu tortor.</div>
												<div class="text-muted small mt-1">15m ago</div>
											</div>
										</div>
									</a>
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<img src="img/avatars/avatar-2.jpg" class="avatar img-fluid rounded-circle" alt="William Harris">
											</div>
											<div class="col-10 ps-2">
												<div class="text-dark">William Harris</div>
												<div class="text-muted small mt-1">Curabitur ligula sapien euismod vitae.</div>
												<div class="text-muted small mt-1">2h ago</div>
											</div>
										</div>
									</a>
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<img src="img/avatars/avatar-4.jpg" class="avatar img-fluid rounded-circle" alt="Christina Mason">
											</div>
											<div class="col-10 ps-2">
												<div class="text-dark">Christina Mason</div>
												<div class="text-muted small mt-1">Pellentesque auctor neque nec urna.</div>
												<div class="text-muted small mt-1">4h ago</div>
											</div>
										</div>
									</a>
									<a href="#" class="list-group-item">
										<div class="row g-0 align-items-center">
											<div class="col-2">
												<img src="img/avatars/avatar-3.jpg" class="avatar img-fluid rounded-circle" alt="Sharon Lessman">
											</div>
											<div class="col-10 ps-2">
												<div class="text-dark">Sharon Lessman</div>
												<div class="text-muted small mt-1">Aenean tellus metus, bibendum sed, posuere ac, mattis non.</div>
												<div class="text-muted small mt-1">5h ago</div>
											</div>
										</div>
									</a>
								</div>
								<div class="dropdown-menu-footer">
									<a href="#" class="text-muted">Show all messages</a>
								</div>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-toggle="dropdown">
								<i class="align-middle" data-feather="settings"></i>
							</a>

							<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-toggle="dropdown">
								<img src="img/avatars/avatar.svg" class="avatar img-fluid rounded me-1" alt='Foto Perfil' /> <span class="text-dark"><?=$_SESSION[NAME.'_cuenta']['usuario']; ?></span>
							</a>
							<div class="dropdown-menu dropdown-menu-end">
								<a class="dropdown-item" href="pages-profile.html"><i class="align-middle me-1" data-feather="user"></i> Profile</a>
								<a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="pie-chart"></i> Analytics</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="index.html"><i class="align-middle me-1" data-feather="settings"></i> Settings & Privacy</a>
								<a class="dropdown-item" href="#"><i class="align-middle me-1" data-feather="help-circle"></i> Help Center</a>
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="index.php?c=login&a=cerrarSession">Log out</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link" href="index.php?c=login&a=cerrarSession">
								<i class="align-middle" data-feather="power"></i>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
<script src="resources/js/app.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	// alert("asd");
});
</script>
	
