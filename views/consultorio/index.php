<?php
        //require_once "models/ConsultorioModel.php";
        //primero obtenemos el parámetro que nos dice en qué página estamos.
        $page = 1;     //inicializamos la variable $page a 1 por default
        if (array_key_exists('pg', $_GET)) {
        //si el valor pg existe en nuestra url, significa que estamos en una pagina en especifico.    
        $page = $_GET['pg']; 
        }
        //ahora que tenemos en que pagina estamos obtengamos los resultados:
        // a) el numero de registros en la tabla
      
        $conteo = $this->mode->contarActores();
        //ahora dividimos el conteo total por el número de registros que queremos por página.
        //en esto caso 18
        $max_num_paginas = intval($conteo / 18);
        
        // ahora obtenemos el segmento paginado que corresponde a esta página
        
        $segmento = $this->mode->actoresPagina($page);
        //ya tenemos el segmento, ahora vamos a mostrar resultados.
?>

<!--<nav class="navbar navbar-expand-lg navbar-light bg-light">
<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="navbar-brand" href="#">Ansamar</a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Dropdown</a>
    <ul class="dropdown-menu">
      <li><a class="dropdown-item" href="#">Acción</a></li>
      <li><a class="dropdown-item" href="#">Otra acción</a></li>
      <li><a class="dropdown-item" href="#">Algo más aquí</a></li>
      <li><hr class="dropdown-divider"></li>
      <li><a class="dropdown-item" href="#">Enlace separado</a></li>
    </ul>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Enlace</a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled">Deshabilitado</a>
  </li>
</ul>
</nav>-->


<!--<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#"><h3>Ansamar</h3></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Features</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Pricing</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown link
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>-->

<link href="https://cdn.datatables.net/1.13.7/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/2.4.2/css/buttons.bootstrap5.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<p></p>

<div class="col-md-8">
  <?php if($this->accesoRegistrar){ ?>
  <button type="button" class="btn btn-outline-success" id="abrirModal">
  Registrar
  </button>
  <?php } ?>
</div>
<p></p>
<!-- FIN QUE ACTIVA MODAL -->



<!-- BOTON BUSCAR -->
                  <div class="col-md-8">
                    <?php if($this->accesoConsultar){ ?>
                        <div class="input-group">
                            <input type="search" onKeyUp="buscar();" name="busqueda" id="busqueda" class="form-control rounded"  autocomplete="off" placeholder="Nombre del consultorio" aria-label="Search" aria-describedby="search-addon" maxlength="50" />
                        
                          <button type="button" class="btn btn-outline-info">
                          <i class="bi bi-search"></i>
                          </button>
                        </div>
                         <?php } ?>
                </div>
<p></p>
<!-- MODAL PARA REGISTRAR -->
<div class="modal fade" style="overflow-y: scroll;" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
<div class="card-body">

                        <div class="alert alert-success" role="alert">
                            <h3>Registrar consultorio</h3>
                       
                        </div>
                       
                    <form class="form-horizontal" method="post" id="formulario" name="formulario" action="">
                     
                        <input type="hidden" name="urlActual" id="urlActual" value="index.php?c=consultorio&a=guardar">
                        <div class="col-md-40">
                            </label><b>Nombre:</b></label>
                            <input type="text area" id="descripcion" name="descripcion" onkeypress="return permite(event, 'num_car')" onKeyUp="buscarReg();" class="form-control" value="" aria-describedby="emailHelp" placeholder="Ejemplo. Dental la 48" maxlength="50" required>
                        <input type="hidden" name="inputVerificarReg" id="inputVerificarReg" value="descripcion">
              
                        </div>
                        <div id="verificarRegistro"></div>
            
                        <div class="col-md-40">
                        </label><b>Dirección:</b></label>
                        <textarea id="direccion" name="direccion" class="form-control" id="exampleFormControlTextarea1" value="" rows="4" placeholder="Ejemplo. Carrera 13A entre 3 y 5 San Francisco" maxlength="100" required></textarea>
                        </div>
          
                        <div class="col-md-40">
                        </label><b>Cantidad de sillón dental:</b></label>
                       
                        <select class="form-select form-select-lg mb-1" name="sillas" id="sillas" class="form-select form-select-lg mb-1" required>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                        </div>
              
            <div class="row">
 
              <div class="form-group col-md-6">
                <label for="nombre"><b>Número teléfono:</b></label>
                <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="" aria-describedby="emailHelp" placeholder="04245208619" maxlength="11" required>
              </div>
            </div>                      
                                
</div>
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="btnguardar" id="btnguardar" class="btn btn-outline-success">Registrar</button>
        <button type="button" class="btn btn-sebtn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- FIN DEL MODAL -->
<div class="row" style="overflow-x: scroll;">
<div id="resultadoBusqueda"></div>
<input type="hidden" id="controlador" value="consultorio">
<div id="tabla"></div>
</div>


<div style="text-align: center; font-size: 30px">
            <?php
//ahora viene la parte importante, que es el paginado
//recordemos que $max_num_paginas fue previamente calculado.

            for ($i = 0; $i < $max_num_paginas; $i++) {
                echo '<a href="index.php?pg=' . ($i + 1) . '">' . ($i + 1) . '</a> - ';
            }
            ?>
  </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.7/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.print.min.js"></script>



<!-- COMIENZO DE TABLA QUE MUESTRA LOS REGISTROS -->

<!-- FUNCICON JS PARA BUSCAR REGISTROS -->

<!--<script>
$(document).ready(function() {
    $("#resultadoBusqueda").html('');
});

function buscar() {
    var textoBusqueda = $("input#busqueda").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=consultorio&a=buscarRegistro", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#resultadoBusqueda").html('');
        };
};
</script>-->
<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL CONSULTORIO -->

<!--<script>
$(document).ready(function() {
    $("#verificarRegistro").html('');
});

function buscarReg() {
    var textoBusqueda = $("input#descripcion").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=consultorio&a=verificarRegistroConsultorio", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#verificarRegistro").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#verificarRegistro").html('');
        };
};
</script>-->


<!--<script type="text/javascript">
  $( function() {
    $("#codTlfno").change( function() {
        if ($(this).val() == "5") {
            $("#codTlfno1").prop("disabled", false);
        } else {
            $("#codTlfno1").prop("disabled", true);
        }
    });
});
</script>-->

<!--<script>
setInterval( function(){

$('#tablaConsultorio').load('index.php?c=consultorio&a=cargarTablaConsultorios');

},3000)
</script>-->

<!--<script type="text/javascript">
  $(document).ready(function(){
$('#btnguardar').click(function(){
  var datos=$('#formulario').serialize();

    $.ajax({
      type:"POST",
      url:"index.php?c=consultorio&a=guardar",
      data:datos,
      success:function(r){
        if(r==1){
 
          swal("Excelente", "Registro Exitoso", "success")
          $('#staticBackdrop').modal('hide');
        }else if(r==2){

          swal("Atención!", "Error en registro", "error")

        }else {
          swal("Atención!", "Complete los campos", "error")
        }
      }

    });
    return false;
  }); 
});
</script>-->

<!--<script>
function abrirModal() {
    
    $('#staticBackdrop').modal('show');
};
</script>-->

    
