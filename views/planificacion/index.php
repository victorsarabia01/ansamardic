


<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  
  <div class="card-body">
     
                        <div class="alert alert-success" role="alert">
                            <h3>Planificación mensual de atención en consultorios</h3>
                        </div>

                    <?php if($this->accesoRegistrar){ ?>  
                    <form class="form-horizontal" method="post" action="index.php?c=planificacion&a=guardar">
              

                        <div class="col-md-8">
                           
                             <select name="consultorio" id="consultorio" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Consultorio</option>
                                <?php foreach ($this->mode->Consultar("listarTodosConsultorios")  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->descripcion ?></option>
                                <?php endforeach ?>
                            </select>
                           
                            <select name="turno" id="turno" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Turno</option>
                                <option value="1">Mañana</option>
                                <option value="2">Tarde</option>
                            </select>
                    
                            <select name="doctor" id="doctor" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                              <option value="0">Odontologo</option>
                                <?php foreach ($this->mode->Consultar("listarTodosDoctores")  as $k) : ?>
                                    <option value="<?php echo $k->id_empleado ?>"> <?php echo $k->nombres." ".$k->apellidos?></option>
                                <?php endforeach ?>
                            </select>
                           
                        </div>

                        
                        </div>
                        <div class="col-md-8">
                        <div class="btn-group" role="group" aria-label="Basic checkbox toggle button group" id="prueba">
                        <input type="checkbox" class="btn-check" value="1" name="btncheck1" id="btncheck1" autocomplete="off">
                        <label class="btn btn-outline-primary" for="btncheck1">Lunes</label>
                        <input type="checkbox" class="btn-check" name="btncheck2" id="btncheck2" value="1" autocomplete="off">
                        <label class="btn btn-outline-primary" for="btncheck2">Martes</label>
                        <input type="checkbox" value="1" class="btn-check" id="btncheck3" name="btncheck3" autocomplete="off">
                        <label class="btn btn-outline-primary" for="btncheck3">Miercoles</label>
                    
                        <input type="checkbox" value="1" class="btn-check" id="btncheck4" name="btncheck4" autocomplete="off">
                        <label class="btn btn-outline-primary" for="btncheck4" >Jueves</label>
                        <input type="checkbox" value="1" class="btn-check" id="btncheck5" name="btncheck5" autocomplete="off">
                        <label class="btn btn-outline-primary" for="btncheck5">Viernes</label>
                        <input type="checkbox" value="1" class="btn-check" id="btncheck6" name="btncheck6" autocomplete="off">
                        <label class="btn btn-outline-primary" for="btncheck6">Sabado</label>
                        </div>
                  
                            <button type="submit" href="?c=guardar" value="Guardar" name="registrar" id="registrar" class="btn btn-outline-success">Guardar planificación</button>
                      
                     
                        
            </form>

  </div>
  <?php } ?>
</div>
</div>

<!-- BUTTON BUSCAR -->
<div class="container">
	
                <div class="col-md-8">
                        <div class="input-group">
                            <input type="search" onKeyUp="buscar();" name="busqueda" id="busqueda" class="form-control rounded mayusculas buscar" autocomplete="off" placeholder="Nombre del consultorio" aria-label="Search" aria-describedby="search-addon" maxlength="50" />
                        
                          <button type="button" class="btn btn-outline-info">
                          <i class="bi bi-search"></i>
                          </button>
                        </div>
                </div>
              </div>  
                       
		<p></p>
<!-- BUTTON BUTTON BUSCAR -->

<!-- RESULTADO BUSQUEDA-->
<div class="row" style="overflow-x: scroll;">
<input type="hidden" id="controlador" value="planificacion">
<div id="resultadoBusqueda"></div>
<div id="tabla"></div>
</div>






