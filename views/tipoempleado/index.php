
<!-- BOTON QUE ACTIVA MODAL -->.
<div class="col-md-8">
    <?php if($this->accesoRegistrar){ ?>
        <button type="button" id="abrirModal" class="btn btn-outline-success">
            Registrar
        </button>
    <?php } ?>
</div>
<p></p>
<!-- FIN QUE ACTIVA MODAL -->



<!-- BOTON BUSCAR -->
    <div class="col-md-8">
        <?php if($this->accesoConsultar){ ?>
            <div class="input-group">
                <input type="search" onKeyUp="buscar();" name="busqueda" id="busqueda" class="form-control rounded mayusculas buscar"  autocomplete="off" placeholder="Tipo de empleado" aria-label="Search" aria-describedby="search-addon" maxlength="50" />
                <button type="button" class="btn btn-outline-info">
                <i class="bi bi-search"></i>
                </button>
            </div>
        <?php } ?>
    </div>
<p></p>
<!-- MODAL PARA REGISTRAR -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
<div class="card-body">

                        <div class="alert alert-success" role="alert">
                            <h3>Tipo de empleado</h3>
                       
                        </div>
                       
                    <form class="form-horizontal" method="post" id="formulario" action="">
                    <input type="hidden" id="urlActual" name="urlActual" value="index.php?c=tipoempleado&a=guardar">    
                        <p></p>
                        <div class="col-md-40">
                            </label><b>Nombre del tipo de empleado:</b></label>
                            <p></p>
                            <input type="text area" id="descripcion" name="descripcion" onkeypress="return permite(event, 'num_car')" onKeyUp="buscarReg();" class="form-control mayusculas buscar" value="" aria-describedby="emailHelp" placeholder="Nombre" maxlength="50" required>
                            <input type="hidden" id="inputVerificarReg" name="inputVerificarReg" value="descripcion">
                        </div>
                        <p></p>
                        <div id="verificarRegistro"></div>
</div>
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="submit" href="#" name="btnguardar" id="btnguardar" class="btn btn-outline-success">Registrar</button>
        <button type="button" class="btn btn-sebtn btn-outline-danger" data-bs-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
</form>
<!-- FIN DEL MODAL -->
<div id="resultadoBusqueda"></div>
<input type="hidden" id="controlador" name="controlador" value="tipoEmpleado">
<div id="tabla"></div>

<!-- FUNCICON JS PARA BUSCAR REGISTROS -->

<!--<script>
$(document).ready(function() {
    $("#resultadoBusqueda").html('');
});

function buscar() {
    var textoBusqueda = $("input#busqueda").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=tipoempleado&a=buscarRegistro", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#resultadoBusqueda").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#resultadoBusqueda").html('');
        };
};
</script>-->
<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL CONSULTORIO -->

<!--<script>
$(document).ready(function() {
    $("#verificarRegistroTipo").html('');
});

function buscarTipo() {
    var textoBusqueda = $("input#descripcion").val();
    
     if (textoBusqueda != "") {
        $.post("index.php?c=tipoempleado&a=verificarRegistroTipo", {valorBusqueda: textoBusqueda}, function(mensaje) {
            $("#verificarRegistroTipo").html(mensaje);
            //$("#idProducto").html(mensaje1);
            //html(mennsaje1);
         }); 
     } else { 
        $("#verificarRegistroTipo").html('');
        };
};
</script>-->

<!--Función de cargar la tabla principal del tipo de empleado-->
<!--<script>
setInterval( function(){

$('#resultadoBusquedaTipoEmpleado').load('index.php?c=tipoempleado&a=cargarTablaTipoEmpleado');

},3000)
</script>-->

