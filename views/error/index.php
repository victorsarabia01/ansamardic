

<div class="row" style="overflow-x: scroll;">
    <div class="container">
        <div class="col-md-12 text-center">
            <br><br>
            <h3 style="text-align:left;">Error</h3>
        </div>
        <br><br>
        <div class="col-md-12 text-center">
            <h2>¡Error 404!</h2>
            <h2>¡Pagina no encontrada!</h2>
            <p>La página que estás buscando no existe, fue removida, su nombre cambió o está temporalmente inhabilitada; disculpe las molestias.</p>
            <br>
            <button class="btn btn-danger" onclick="history.back();">Volver</button>
        </div>
    </div>
</div>


<!-- FUNCICON JS PARA BUSCAR REGISTROS SI EXISTE EL PACIENTE BUSCADO -->
<script>
$(document).ready(function() {
});
</script>

<?php die(); ?>

