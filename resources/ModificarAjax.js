$(document).ready(function(){

  $('#btnmodificar').click(function(){
    var url = $("#accion").val();
    var controlador = $("#controlador").val();
    var datos=$('#formulario').serialize();
    //console.log('prueba');
    //alert(url);
    $.ajax({
      type:"POST",
      url:url,
      data:datos,
      success:function(r){
        if(r==1){
 
          swal("Excelente", "Modificacion Exitosa", "success")
          setTimeout( function() { window.location.href = `index.php?c=${controlador}`; }, 1000 );
          
        }else if(r==2){

          swal("Atención!", "Ya existe una planificacion con los datos Seleccionados", "error")

        }else if(r==3){

          swal("Atención!", "Odontologo ya posee el turno en otro consultorio", "error")

        }else if(r==4){

          swal("Atención!", "No hay sillas disponibles", "error")

        }else if(r==5){

          swal("Atención!", "Mes o dia Incorrecto", "error")

        }else if(r==6){

          swal("Atención!", "Seleccione otro turno o dia diferente", "error")

        }else if(r==7){

          swal("Atención!", "Seleccione una fecha correcta", "error")

        }else if(r==8){

          swal("Atención!", "Error en Modificacion", "error")

        }else {
          swal("Atención!", "Complete los campos", "error")
        }
      }

    });
    return false;
  }); 
});

