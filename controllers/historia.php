<?php
	include_once "controller.php";
    class historiaController extends controller {
		public $mode;
		// public $paciente;
		public $rol;
		public $consulta;
		public $citas;
		public $accesos;
		public $nameControl = "Odontograma";
		public $accesoRegistrar=false;
		public $accesoConsultar=false;
		public $accesoModificar=false;
		public $accesoEliminar=false;
		
		public function __construct(){
			require_once "models/rolModel.php";
			// require_once "models/pacienteModel.php";
			// require_once "models/citaModel.php";
			require_once "models/historiaModel.php";
			$this->mode = new historia_model();
			$this->alm = new historia_model();
			// $this->paciente = new paciente_model();
			// $this->cita = new cita_model();

			$this->rol = new rol_model();
			$idRol=$_SESSION[NAME.'_cuenta']['id_rol'];
			$this->accesos = $this->rol->Consultar("cargarAccesos", $idRol);
			foreach ($this->accesos as $acc) {
				if($acc->nombre_modulo==$this->nameControl){
					if($acc->nombre_permiso=="Registrar"){ $this->accesoRegistrar = true; }
					if($acc->nombre_permiso=="Consultar"){ $this->accesoConsultar = true; }
					if($acc->nombre_permiso=="Modificar"){ $this->accesoModificar = true; }
					if($acc->nombre_permiso=="Eliminar"){ $this->accesoEliminar = true; }
				}
			}
		}
		
		public function index(){
			if($this->accesoConsultar){
				if(!empty($_GET['ci'])){
					$this->consulta = $this->mode->Consultar("BuscarPacienteCita", $_GET['ci']);
					if(count($this->consulta)>0){
						$this->consulta = $this->consulta[0];
					}
				}
				if(!empty($_GET['cita']) && !empty($_GET['paciente'])){
					$this->consulta = $this->mode->Consultar("BuscarPacienteCita", $_GET['paciente']);
					if(count($this->consulta)>0){
						$this->consulta = $this->consulta[0];
					}
					$this->citas = $this->mode->Consultar("listarHistoriaOdontograma", $_GET['cita'], $_GET['paciente']);
				}
				return $this->vista("historia");
			}else{
				return $this->vista("error");
			}
		}

		// GUARDAR EMPLEADO
		public function guardarhistoria(){
			if($this->accesoRegistrar){
				// print_r($_POST);
				// $this->alm->cedula_paciente = $_POST['cedula_paciente'];
				$this->alm->id_paciente = 0;
				$this->alm->id_cita = 0;
				// $busqueda = $this->mode->Consultar("ValidarPaciente", $this->alm->cedula_paciente);
				// if(count($busqueda)>0){
				// 	$this->alm->id_paciente = $busqueda[0]->id;
				// }
				// if($this->alm->id_paciente>0){
					$this->alm->id_cita = $_POST['cita'];
					$busqueda = $this->mode->Consultar("ValidarCita", $this->alm->id_cita);
					if(count($busqueda)>0){
						$this->alm->cedula_paciente = $busqueda[0]->cedula;
						$this->alm->id_paciente = $busqueda[0]->id_paciente;
					}
					if($this->alm->id_cita > 0){
						$this->alm->fecha = date("Y-m-d");
						if(count($_POST['dientes'])>0){
							$dientes = $_POST['dientes'];
							$caras = $_POST['caras'];
							$enfermedades = $_POST['enfermedades'];
							for ($i=0; $i < count($dientes); $i++){
								$this->alm->diente = $dientes[$i];
								$this->alm->cara = $caras[$i];
								$busqueda = $this->mode->Consultar("ValidarEnfermedad", $enfermedades[$i]);
								if(count($busqueda)>0){
									$this->alm->id_enfermedad = $busqueda[0]->id;
									// echo $this->alm->cedula_empleado." ".$this->alm->id_cita." ".$this->alm->diente." ".$this->alm->cara." ".$this->alm->id_enfermedad."\n";
									$this->mode->Registrar("registrarHistoria",$this->alm);
								}
								$stat = "1";
								// $mensaje = "Historia registrada";
								// echo "<script>
									// 	alert('Historia registrada');
									// 	setTimeout( function() { window.location.href = 'index.php?c=historia&cita=".$this->alm->id_cita."&paciente=".$this->alm->cedula_paciente."'; }, 1000 );
								// </script>";
							}
						}else{
							// $mensaje = "Falta seleccionar dientes, caras y enfermedades del paciente";
							$stat = "2";
							// echo "<script>
								// 	alert('Falta seleccionar dientes, caras y enfermedades del paciente');
								// 	history.back();
							// </script>";
						}
					}else{
						$stat = "3";
						// $mensaje = "El paciente no fue encontrado";
						// echo "<script>
							// 	alert('El paciente no fue encontrado');
							// 	history.back();
						// </script>";
					}
				// }else{
					// $mensaje = "El paciente no fue encontrado";
					// $stat = "3";
					// echo "<script>
						// 	alert('El paciente no fue encontrado');
						// 	history.back();
					// </script>";
				// }
				// echo json_encode(['msj'=>$mensaje, 'stat'=>$stat]);
				echo $stat;
			}else{
				return $this->vista("error");
			}
		}

		public function guardarServicios(){
			if($this->accesoRegistrar){
				$cita = $_POST['cita'];
				$cedula = $_POST['cedula_paciente'];
				$id_historia = $_POST['id_historia'];
				$id_servicio = $_POST['id_servicio'];

				foreach ($this->mode->Consultar("ValidarHistoriaServicio", $id_historia, $id_servicio) as $k){
					$this->alm->consulta = $k->consulta;
				}
				if($this->alm->consulta==""){
					$this->alm->id_historia=$id_historia;
					$this->alm->id_servicio=$id_servicio;
					$this->mode->Registrar("registrarHistoriaServicio", $this->alm);
					echo "1";
				}else{
					echo "3";
				}
			}else{
				return $this->vista("error");
			}
		}

		public function actualizarServicios(){
			if($this->accesoRegistrar){
				$ids = $_POST['ids'];
				$evoluciones = $_POST['evolucion'];
				$observaciones = $_POST['observacion'];
				$indicaciones = $_POST['indicaciones'];
				$index = 0;
				for ($i=0; $i < count($ids); $i++) { 
					$this->alm->id_detalle = $ids[$i];
					$this->alm->evolucion = $evoluciones[$i];
					$this->alm->observacion = $observaciones[$i];
					$this->alm->indicacion = $indicaciones[$i];
					$this->mode->Modificar("actualizarHistoriaServicio", $this->alm);
					$index++;
				}
				if($index==count($ids)){
					echo "1";
					// echo "<script>
					// 	alert('Historia guardada y actualizada');
					// 	setTimeout( function() { window.location.href = 'index.php?c=historia'; }, 1000 );
					// </script>";
				}else{
					echo "2";
					// echo "<script>
					// 	alert('No se pudo actualizar');
					// 	history.back();
					// </script>";
				}
			}else{
				return $this->vista("error");
			}
		}

		public function cargarTablaServicios(){
			if($this->accesoConsultar){
				$cita = $_GET['cita'];
				$cedula = $_GET['paciente'];
				$mensaje = "";
				$totalPrecio = 0;
				foreach ($this->mode->Consultar("listarHistoriaServicios", $cita, $cedula) as $k){
					$mensaje .= "
						<tr>
							<td style='text-align:center;'>
								".$k->pieza_dental." ".$k->posicion_dental." ".$k->enfermedad."<br>
								(".$k->fecha_historia.")
							</td>
							<td>".ucwords(mb_strtolower($k->tratamiento))."</td>
							<td>".ucwords(mb_strtolower($k->descripcion))."</td>
							<td>$".number_format($k->precio,2,',','.')."</td>
							<td><textarea name='evolucion[]' class='evolucion' id='evolucion".$k->id."'>".$k->evolucion."</textarea></td>
							<td><textarea name='observacion[]' class='observacion' id='observacion".$k->id."'>".$k->observacion."</textarea></td>
							<td><textarea name='indicaciones[]' class='indicaciones' id='indicaciones".$k->id."'>".$k->indicaciones."</textarea></td>
							<td><input type='hidden' name='ids[]' id='ids".$k->id."' value='".$k->id."'><input type='button' class='btn btn-info eliminarServicio' id='".$k->id."' value='Eliminar'></td>
						</tr>
					";
					$totalPrecio += $k->precio;
				}
				$mensaje .= "
					<tr>
						<td></td>
						<td></td>
						<td>Total: </td>
						<td><b>$".number_format($totalPrecio, 2,',','.')."</b></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				";
				$mensaje .= "
					<script type='text/javascript'>
						$(document).ready(function(){
							$('.eliminarServicio').click(function(e){
								console.log();
								e.preventDefault();
								var id = $(this).attr('id');
								$.ajax({
									url: '?c=historia&a=eliminarServicios',
									type: 'POST',
									data: {
										cedula_paciente: {$cedula},
										cita: {$cita},
										id: id,
									},
									success: function(resp){
										// console.log(resp);
										// alert(resp);
										$('.listaServiciosAplicados').load('index.php?c=historia&a=cargarTablaServicios&cita={$cita}&paciente={$cedula}');
									},
									error: function(respuesta){
										// var datos = JSON.parse(respuesta);
										// console.log(datos);
									}
								});
							});
						});
					</script>
				";
				echo $mensaje;
			}else{
				return $this->vista("error");
			}
		}

		public function eliminarServicios(){
			if($this->accesoConsultar){
				$cita = $_POST['cita'];
				$cedula = $_POST['cedula_paciente'];
				$id = $_POST['id'];
				$result = $this->mode->Eliminar("eliminarServicio", $id);
				echo $result;
			}else{
				return $this->vista("error");
			}
		}
		
	}

		

	
	
?>