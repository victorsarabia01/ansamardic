<?php

include_once "controller.php";
//require('fpdf186/fpdf.php');



	class citaController extends controller {
		
		/*public $mode;
		public $rol;
		public $accesos;*/
		public $nameControl = "Cita";
		/*public $accesoRegistrar=false;
		public $accesoConsultar=false;
		public $accesoModificar=false;
		public $accesoEliminar=false;*/

		public function __construct(){
			require_once "models/rolModel.php";
			require_once "models/citaModel.php";
			$this->mode = new cita_model();
			$this->alm = new cita_model();
			//$this->pdf=new FPDF();
			$this->rol = new rol_model();
			$idRol=$_SESSION[NAME.'_cuenta']['id_rol'];
			$this->accesos = $this->rol->Consultar("cargarAccesos", $idRol);
			foreach ($this->accesos as $acc) {
				if($acc->nombre_modulo==$this->nameControl){
					if($acc->nombre_permiso=="Registrar"){ $this->accesoRegistrar = true; }
					if($acc->nombre_permiso=="Consultar"){ $this->accesoConsultar = true; }
					if($acc->nombre_permiso=="Modificar"){ $this->accesoModificar = true; }
					if($acc->nombre_permiso=="Eliminar"){ $this->accesoEliminar = true; }
				}
			}
		}
		
		public function index(){
			if($this->accesoConsultar){
				return $this->vista("cita");
			}else{
				return $this->vista("error");
			}
		}

		public function editar(){
			if($this->accesoModificar){
				$this->alm = $this->mode->Consultar("cargarInformacionCita", $_REQUEST['id']);
				return $this->vista("cita/modificar");
			}else{
				return $this->vista("error");
			}
		}
		
		//GUARDAR REGISTRO DEL CLIENTE
		public function guardar(){
			if($this->accesoRegistrar){
			
				$this->alm->id_consultorio = $_POST['consultorio'];
				$this->alm->id_turno = $_POST['turno'];
				$this->alm->id_doctor = $_POST['cargarOdontologos'];
				$this->alm->fechaCita = $_POST['fecha'];
				$this->alm->id_paciente = $_POST['resultadoBusquedaPaciente'];

				
				$fechaSeleccionada = date($this->alm->fechaCita);
				$fechaFormat = strtotime($fechaSeleccionada);
				$mes = date ('m',$fechaFormat);
				$mesActual=date("m");
				
				$this->alm->mesRegistro = date ('m',$fechaFormat);
				//$this->alm->mesRegistro = '10';

				$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
				$diaSeleccionado= $dias[date('w',$fechaFormat)];
				$diaSemana = date ("N",$fechaFormat);

				$hoy = date("Y-m-d");
				$fechaFormulario = $fechaSeleccionada;
				foreach ($this->mode->Consultar("verificarDiaSemana", $_POST['consultorio'],$_POST['turno'],$_POST['cargarOdontologos'],$diaSemana) as $k){
						$this->alm->verificarDia = $k->verificarDia;
				}
				// Si la fecha es de apartir de hoy => true 
				if ($hoy <= $fechaFormulario) {

				foreach ($this->mode->Consultar("verificarDiaSemana", $_POST['consultorio'],$_POST['turno'],$_POST['cargarOdontologos'],$diaSemana) as $k){
						$this->alm->verificarDia = $k->verificarDia;
				}

				if($diaSeleccionado != $this->alm->verificarDia){
						echo "4";
						//$this->mode->Registrar("registrarCita", $this->alm);
						
				}else{
							foreach ($this->mode->Consultar("contadorCitas", $_POST['fecha'],$_POST['turno'],$_POST['consultorio']) as $k){
								$this->alm->contador = $k->contador;
							}
							foreach ($this->mode->Consultar("verificarExistenciaCita", $_POST['fecha'],$_POST['resultadoBusquedaPaciente']) as $k){
								$this->alm->verificarExistenciaCita = $k->verificarExistenciaCita;
							}
							if($this->alm->contador<=1 && $this->alm->verificarExistenciaCita == ""){
								
								$this->mode->Registrar("registrarCita", $this->alm);

								echo "1";

								/*$this->pdf->AddPage();
								$this->pdf->SetFont('Arial','B',16);
								$this->pdf->Cell(40,10,'¡Mi primera página pdf con FPDF!');
								$this->pdf->Output();*/
																
								
								
							}else{
								echo "5";
							}
				}		
				
				}// FIN DEL IF FECHA CORRECTA
				else{

					echo "6";
				}
				}// FIN DEL IF PERMISO REGISTRAR
				else{
				return $this->vista("error");
				}
		}// FIN DE CLASE

		public function modificar(){
			if($this->accesoModificar){
				$alm = new cita_model();
		
				$alm->id = $_POST['id'];
				//$alm->resultadoBusquedaPaciente = $_POST['id'];
				$alm->id_consultorio = $_POST['consultorio'];
				$alm->id_turno = $_POST['turno'];
				$alm->id_doctor = $_POST['cargarOdontologos'];
				$alm->fechaCita = $_POST['fecha'];

				$fechaSeleccionada = date($alm->fechaCita);
				$fechaFormat = strtotime($fechaSeleccionada);
				$mes = date ('m',$fechaFormat);
				$mesActual=date("m");
				$alm->mesRegistro = date ('m',$fechaFormat);
				$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
				$diaSeleccionado= $dias[date('w',$fechaFormat)];
				$diaSemana = date ("N",$fechaFormat);
				$hoy = date("Y-m-d");
				$fechaFormulario = $fechaSeleccionada;
				// Si la fecha es de apartir de hoy => true 
				if ($hoy <= $fechaFormulario) {
					foreach ($this->mode->Consultar("verificarDiaSemana", $_POST['consultorio'],$_POST['turno'],$_POST['cargarOdontologos'],$diaSemana) as $k){
						$alm->verificarDia = $k->verificarDia;
					}

					if($diaSeleccionado != $alm->verificarDia){
						echo "5";
						
					}else{
					//echo "1";	
					
					foreach ($this->mode->Consultar("contadorCitas", $_POST['fecha'],$_POST['turno'],$_POST['consultorio']) as $k){
							$alm->contador = $k->contador;
					}
					foreach ($this->mode->Consultar("verificarExistenciaCita", $_POST['fecha'],$_POST['resultadoBusquedaPaciente']) as $k){
								$this->alm->verificarExistenciaCita = $k->verificarExistenciaCita;
					}
						if($alm->contador<=1 && $alm->verificarExistenciaCita == ""){
							//$this->mode->Modificar("modificarCita", $alm);
							echo "1";
							
						}else{
							echo "6";
							
						}
			
					}
					}else{
					echo "7";			
					}
				
					}else{
					return $this->vista("error");
					}
				}

		public function modificar1(){
			if($this->accesoModificar){
				$alm = new cita_model();
				//$alm->cedula_cliente = $this->mode->Consultar("cargarCedula", $_POST['cedula']);

				$alm->id = $_POST['id'];
				//$alm->cedula = $_POST['cedula1'];
				$alm->id_consultorio = $_POST['consultorio'];
				$alm->id_turno = $_POST['turno'];
				$alm->id_doctor = $_POST['cargarOdontologos'];
				$alm->fechaCita = $_POST['fecha'];
				//$alm->correo = $_POST['correo'];

				$fechaSeleccionada = date($alm->fechaCita);
				$fechaFormat = strtotime($fechaSeleccionada);
				$mes = date ('m',$fechaFormat);
				$mesActual=date("m");
				$alm->mesRegistro = date ('m',$fechaFormat);
				$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado");
				$diaSeleccionado= $dias[date('w',$fechaFormat)];
				$diaSemana = date ("N",$fechaFormat);

				$hoy = date("Y-m-d");
				$fechaFormulario = $fechaSeleccionada;
				// Si la fecha es de apartir de hoy => true 
				if ($hoy <= $fechaFormulario) {
					foreach ($this->mode->Consultar("verificarDiaSemana", $_POST['consultorio'],$_POST['turno'],$_POST['cargarOdontologos'],$diaSemana) as $k){
						$alm->verificarDia = $k->verificarDia;
					}

					if($diaSeleccionado != $alm->verificarDia){
						echo "5";
						/*echo   "<script type='text/javascript'>  
							//alertify.error('no se pudo Registrar');
							alert('Mes o dia Incorrecto');
							history.back();
							//setTimeout( function() { window.location.href = 'index.php?c=cita'; }, 1000 );
						</script>";*/
					}else{
						foreach ($this->mode->Consultar("cargarIdPaciente", $_POST['cedula1']) as $k){
							$alm->id_paciente = $k->id;
						}
						foreach ($this->mode->Consultar("contadorCitas", $_POST['fecha'],$_POST['turno'],$_POST['consultorio']) as $k){
							$alm->contador = $k->contador;
						}
						foreach ($this->mode->Consultar("verificarExistenciaCita", $_POST['fecha'],$_POST['resultadoBusquedaPaciente']) as $k){
								$this->alm->verificarExistenciaCita = $k->verificarExistenciaCita;
						}
						if($this->alm->contador<=1 && $this->alm->verificarExistenciaCita == ""){
							//$this->mode->Modificar("modificarCita", $alm);
							echo "1";
							/*echo "<script>
								alert('Cita modificada');
								setTimeout( function() { window.location.href = 'index.php?c=cita'; }, 1000 );
								//location.reload();
								//window. history. back();
							</script>";*/
						}else{
							echo "6";
							/*echo "<script>
								alert('SELECCIONE OTRO TURNO O DIA DIFERENTE');
								history.back();
							</script>";*/
						}
					}
				}else{
					echo "7";
					/*echo   "<script>  
						alert('Seleccione una fecha correcta');
						history.back();
					</script>";*/
				}
			}else{
				return $this->vista("error");
			}
		}

		public function eliminar(){
			if($this->accesoEliminar){
				$alm = new cita_model();
				$this->mode->Eliminar("eliminar", $_REQUEST['id']);
				/*echo "<script>
					alert('Cita eliminada');
					setTimeout( function() { window.location.href = 'index.php?c=cita'; }, 1000 );
				</script>";*/
				echo "1";
			}else{
				return $this->vista("error");
			}
		}

		
		public function tablacita(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$mensaje ="";
				$mensaje .='
					<div class="row">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3>Citas registradas del año en curso</h3>
							</div>
						</div>
					</div>

					<div class="col-md-12 text-center">
						<table class="table table-hover">
							<tr class="table-secondary">
								<th>Fecha</th>
								<th>Consultorio</th>
								<th>Turno</th>
								<th>Cédula</th>
								<th>Paciente</th>
								<th>Teléfono</th>
								<th>Correo</th>
								<th>Odontologo</th>';
								if($this->accesoModificar){
									$mensaje .='<th>EDITAR</th>';
								}
									$mensaje .='<th>PDF</th>';
								if($this->accesoEliminar){
									$mensaje .='<th>ELIMINAR</th>';
								}
								$mensaje .='
							</tr>';
							foreach ($this->mode->Consultar("listarCitas") as $k){
								$mensaje .= '
								<tr>
									<td>'.date("d-m-Y", strtotime($k->fecha)).'</td>
									<td><b>'.$k->consultorio.'</b></td>
									<td>'.$k->turno.'</td>
									<td>'.$k->cedula.'</td>
									<td>'.$k->nombres.' '.$k->apellidos.'</td>
									<td>'.$k->tlfno.'</td>
									<td>'.$k->email.'</td>
									<td>'.$k->nombresDoctor.' '.$k->apellidosDoctor.'</td>';
									if($this->accesoModificar){
										$mensaje .='
										<td>
											<a href="index.php?c=cita&a=editar&id='.$k->id.'" type="button" class="btn btn-outline-primary">
												<i class="bi bi-pencil-square"></i>
											</a>
										</td>
										<td>
											<a href="index.php?c=citaPdf&a=pdf&id='.$k->id.'" target="_blank" type="button" class="btn btn-outline-info">
												<i class="bi bi-file-earmark-pdf"></i>
											</a>
										</td>
										';
									}
									if($this->accesoEliminar){
										$mensaje .='
										<td>
											<button href="#" id="'.$k->id.'" type="button" class="btn btn-outline-danger eliminar">
												<i class="bi bi-trash"></i>
											</button>
										</td>
										';
									}
									$mensaje .='
								</tr>';
							}
						$mensaje .= '</table>
					</div>

					<script type="text/javascript">
						$(document).ready(function(){
							$(".eliminar").click(function(e){
								console.log();
								e.preventDefault();
								var id = $(this).attr("id");
								swal({
									title: "Atención!!!",
									text: "¿Esta seguro de eliminar el registro?!",
									type: "warning",
									showCancelButton: true,
									confirmButtonClass: "btn-danger",
									confirmButtonText: "Confirmar",
									cancelButtonText: "Cancelar",
									closeOnConfirm: false,
									closeOnCancel: false
									}, function(isConfirm) {
										if (isConfirm) {
											//Si SE confirma la eliminacion se ejecuta el reenvio al php encargado
											//window.location.href="index.php?c=cita&a=eliminar&id="+id;

										$.ajax({
										      type:"POST",
										      url:"index.php?c=cita&a=eliminar&id="+id,
										
										      success:function(r){
										        if(r==1){
										 
										          swal("Atención!", "Registro Eliminado", "warning")
										          
										        }else {
										          swal("Atención!", "Error al eliminar", "error")
										        }
										      }

										 });


										} else {
											//Si se cancela se emite un mensaje
											swal("Cancelado", "Usted ha cancelado la acción de eliminación", "error");
										}
									}
								);
							});
						});
					</script>
				';
				echo $mensaje;
			}else{
				return $this->vista("error");
			}
		}

		
		public function buscarRegistro(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$mensaje ="";
				$consultaBusqueda = $_POST['valorBusqueda'];
				foreach ($this->mode->Consultar("buscarRegistroCita", $consultaBusqueda) as $resultados){
					$alm->id = $resultados->id;
					

				}
				

				if($alm->id != ""){
					$mensaje .='
					<div class="row">
						<div class="col-md-12 text-center">
							<table class="table table-hover">
								<tr class="table-secondary">
									<thead class="table-success">
										<th>Fecha</th>
										<th>Consultorio</th>
										<th>Turno</th>
										<th>Cédula</th>
										<th>Paciente</th>
										<th>Teléfono</th>
										<th>Correro</th>
										<th>Odontólogo</th>';
										if($this->accesoModificar){
											$mensaje .='<th>EDITAR</th>';
										}
										if($this->accesoEliminar){
											$mensaje .='<th>ELIMINAR</th>';
										}
										$mensaje .='
									</thead>
								</tr>';
										
								foreach ($this->mode->Consultar("buscarRegistroCita", $consultaBusqueda) as $resultados):
									
								$mensaje .='	

								
								<tr>
									<td>'.date("d-m-Y", strtotime($resultados->fecha)).'</td>
									<td>'.$resultados->consultorio.'</td>
									<td>'.$resultados->turno.'</td>
									<td>'.$resultados->cedula.'</td>
									<td>'.$resultados->nombres.' '.$resultados->apellidos.'</td>
									<td>'.$resultados->tlfno.'</td>
									<td>'.$resultados->email.'</td>
									<td>'.$resultados->nombresDoctor.' '.$resultados->apellidosDoctor.'</td>';
									if($this->accesoModificar){
										$mensaje .='
										<td>
											<a href="index.php?c=cita&a=editar&id='.$alm->id = $resultados->id.'" type="button" class="btn btn-outline-primary">
												<i class="bi bi-pencil-square"></i>
											</a>
										</td>
										';
									}
									if($this->accesoEliminar){
										$mensaje .='
										<td>
											<button href="#" id="'.$alm->id = $resultados->id.'" type="button" class="btn btn-outline-danger eliminar">
												<i class="bi bi-trash"></i>
											</button>
										</td>
										';
									}
									endforeach;
									$mensaje .='
								</tr>
							</table>
						</div>
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
							$(".eliminar").click(function(e){
								e.preventDefault();
								var id = $(this).attr("id");
								swal({
									title: "Atención!!!",
									text: "¿Esta seguro de inhabilitar el registro?!",
									type: "warning",
									showCancelButton: true,
									confirmButtonClass: "btn-danger",
									confirmButtonText: "Confirmar",
									cancelButtonText: "Cancelar",
									closeOnConfirm: false,
									closeOnCancel: false
								},
								function(isConfirm) {
									if (isConfirm) {
										//Si SE confirma la eliminacion se ejecuta el reenvio al php encargado
										//window.location.href="index.php?c=cita&a=eliminar&id="+id;

										$.ajax({
										      type:"POST",
										      url:"index.php?c=cita&a=eliminar&id="+id,
										
										      success:function(r){
										        if(r==1){
										 
										          swal("Atención!", "Registro Eliminado", "warning")
										          
										        }else {
										          swal("Atención!", "Error al eliminar", "error")
										        }
										      }

										 });


									} else {
										//Si se cancela se emite un mensaje
										swal("Cancelado", "Usted ha cancelado la acción de eliminación", "error");
									}
								});
							});
						});
					</script>';
					
				}echo $mensaje;
			}else{
				return $this->vista("error");
			}
		}

			
		public function consultarOdontologos(){
			if($this->accesoConsultar){

				$alm = new cita_model();
				$prueba="No existe planificacion";
				$id = filter_input(INPUT_POST, 'consultorio'); 
				$id_turno = filter_input(INPUT_POST, 'turno'); 
				//echo '<option value="0">'.$id_turno.''.$id.'</option>';
				foreach ($this->mode->Consultar("consultarOdontologos", $id,$id_turno) as $k){
					if ($k->nombres != ""){
						echo '<option value="'.$k->id.'">'.$k->nombres.''." ".''.$k->apellidos.'</option>';
					}else{
						echo '<option value="0">'.$prueba.'</option>';
					}	
				}
				if($k->nombres == ""){
					echo '<option value="0">'.$prueba.'</option>';
				}

			}else{
				return $this->vista("error");
			}
		}

		public function consultarOdontologosDias(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				/*$id = filter_input(INPUT_POST, 'consultorio'); 
				$id_turno = filter_input(INPUT_POST, 'turno'); 
				$empleado = filter_input(INPUT_POST, 'empleado');*/
				
				$id = $_POST['consultorio'];
				$id_turno = $_POST['turno'];
				$empleado = $_POST['empleado'];
				if($empleado=='' || $empleado==null){
					$empleado=="0";
				}else{
					foreach ($this->mode->Consultar("consultarOdontologos1", $id,$id_turno,$empleado) as $k){
					echo '<h5><b>- '.$k->dia.'</b></h5>';
				}	
				}
				//echo '<option value="0">'.$id_turno.''.$id.'</option>';
				
			}else{
				return $this->vista("error");
			}
		}

		public function buscarPacienteReg1(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$consultaBusqueda = $_POST['valorBusqueda'];
				foreach ($this->mode->Consultar("buscarRegistroPaciente", $consultaBusqueda) as $resultados){
					$alm->id = $resultados->id;
				}
				if($alm->id != ""){
					/*echo "<div class='col-md-8'>
						<p style='color:Red;'> Paciente Ya registrado </p>
					</div>";*/
					echo '
					<h5><label>Paciente:</label></h5>
					<input type="hidden" id="id_paciente" value="'.$resultados->id.'">
					<h3><b>'.$resultados->nombres.''." ".''.$resultados->apellidos.'</b></h3>
					
					';
				}
			}else{
				return $this->vista("error");
			}
		}

		public function buscarPacienteReg(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$consultaBusqueda = $_POST['valorBusqueda'];
				foreach ($this->mode->Consultar("buscarRegistroPaciente", $consultaBusqueda) as $resultados){
					$alm->id = $resultados->id;
				}
				if($alm->id != ""){
					/*echo "<div class='col-md-8'>
						<p style='color:Red;'> Paciente Ya registrado </p>
					</div>";*/
					echo '<option value="'.$resultados->id.'">'.$resultados->nombres.''." ".''.$resultados->apellidos.'</option>';
				}
			}else{
				return $this->vista("error");
			}
		}

		public function buscarRegistroPaciente(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$consultaBusqueda = $_POST['valorBusqueda'];
				foreach ($this->mode->Consultar("buscarRegistroPaciente", $consultaBusqueda) as $resultados){
					$alm->id = $resultados->id;
				}
				if($alm->id != ""){
					echo "<div class='col-md-8'>
						<p style='color:Red;'> Paciente Ya registradox </p>
					</div>";
				}
			}else{
				return $this->vista("error");
			}
		}

		/*public function consultarTurnos(){
			if($this->accesoConsultar){
				foreach ($this->mode->Consultar("consultarOdontologos", $id,$id_turno) as $k){
					echo '<option value="'.$alm->datos = $k->id.'">'.$alm->datos1 = $k->nombres.'</option>';
				}
			}else{
				return $this->vista("error");
			}
		}*/

		/*public function listarCitasAjax(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$mensaje ="";
				$mensaje .='
					<div class="row">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3>Citas registradas del mes en curso</h3>
							</div>
						</div>
					</div>

					<div class="col-md-12 text-center">
						<table class="table table-hover">
							<tr class="table-secondary">
								<th>Fecha</th>
								<th>Consultorio</th>
								<th>Turno</th>
								<th>Cédula</th>
								<th>Paciente</th>
								<th>Teléfono</th>
								<th>Correo</th>
								<th>Odontologo</th>
								<th>EDITAR</th>
								<th>ELIMINAR</th>
								
							</tr>

							foreach ($this->mode->Consultar("listarCitas") as $k){
								
								<tr>
									<td>'.date("d-m-Y", strtotime($k->fecha)).'</td>
									<td>'.$k->consultorio.'</td>
									<td>'.$k->turno.'</td>
									<td>'.$k->cedula.'</td>
									<td>'.$k->nombres.' '.$k->apellidos.'</td>
									<td>'.$k->tlfno.'</td>
									<td>'.$k->email.'</td>
									<td>'.$k->nombresDoctor.' '.$k->apellidosDoctor.'</td>';
									if($this->accesoModificar){
										$mensaje .='
										<td>
											<a href="index.php?c=cita&a=editar&id='.$k->id.'" type="button" class="btn btn-outline-primary">
												<i class="bi bi-pencil-square"></i>
											</a>
										</td>
										';
									}
									if($this->accesoEliminar){
										$mensaje .='
										<td>
											<button href="#" id="'.$k->id.'" type="button" class="btn btn-outline-danger eliminar">
												<i class="bi bi-trash"></i>
											</button>
										</td>
										';
									}
									$mensaje .='
								</tr>';
							}
						$mensaje .= '</table>
					</div>

					<script type="text/javascript">
						$(document).ready(function(){
							$(".eliminar").click(function(e){
								console.log();
								e.preventDefault();
								var id = $(this).attr("id");
								swal({
									title: "Atención!!!",
									text: "¿Esta seguro de eliminar el registro?!",
									type: "warning",
									showCancelButton: true,
									confirmButtonClass: "btn-danger",
									confirmButtonText: "Confirmar",
									cancelButtonText: "Cancelar",
									closeOnConfirm: false,
									closeOnCancel: false
									}, function(isConfirm) {
										if (isConfirm) {
											//Si SE confirma la eliminacion se ejecuta el reenvio al php encargado
											window.location.href="index.php?c=cita&a=eliminar&id="+id;
										} else {
											//Si se cancela se emite un mensaje
											swal("Cancelado", "Usted ha cancelado la acción de eliminación", "error");
										}
									});
								});
							});
						});
					</script>
				';
				echo $mensaje;
			}else{
				return $this->vista("error");
			}
		}*/

		/*public function buscarRegistroHoy(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$consultaBusqueda = $_POST['valorBusqueda'];
				foreach ($this->mode->Consultar("buscarRegistroCita", $consultaBusqueda) as $resultados){
					$alm->id = $resultados->id;
					$alm->fecha = $resultados->fecha;
					$alm->consultorio = $resultados->consultorio;
					$alm->nombres = $resultados->nombres;
					$alm->cedula = $resultados->cedula;
					$alm->apellidos = $resultados->apellidos;
					$alm->telefono = $resultados->tlfno;
					$alm->turno = $resultados->turno;
					$alm->nombresDoctor = $resultados->nombresDoctor;
					$alm->apellidosDoctor = $resultados->apellidosDoctor;
					$alm->correo = $resultados->email;
				}
				if($alm->id != ""){
					echo '<div class="row">
						<div class="col-md-12 text-center">
							<table class="table table-hover">
								<tr class="table-secondary">
									<thead class="table-success">
										<th>Fecha</th>
										<th>Consultorio</th>
										<th>Turno</th>
										<th>Cédula</th>
										<th>Paciente</th>
										<th>Teléfono</th>
										<th>Correro</th>
										<th>Odontólogo</th>
										<th>EDITAR</th>
										<th>ELIMINAR</th>
									
									</thead>
								</tr>
								<tr>
									<td>'.date("d-m-Y", strtotime($alm->fecha)).'</td>
									<td>'.$alm->consultorio.'</td>
									<td>'.$alm->turno.'</td>
									<td>'.$alm->cedula.'</td>
									<td>'.$alm->nombres.' '.$alm->apellidos.'</td>
									<td>'.$alm->telefono.'</td>
									<td>'.$alm->correo.'</td>
									<td>'.$alm->nombresDoctor.' '.$alm->apellidosDoctor.'</td>';
									if($this->accesoModificar){
										$mensaje .='
										<td>
											<a href="index.php?c=cita&a=editar&id='.$alm->id.'" type="button" class="btn btn-outline-primary">
												<i class="bi bi-pencil-square"></i>
											</a>
										</td>
										';
									}
									if($this->accesoEliminar){
										$mensaje .='
										<td>
											<button href="#" id="'.$alm->id.'" type="button" class="btn btn-outline-danger eliminar">
												<i class="bi bi-trash"></i>
											</button>
										</td>
										';
									}
									$mensaje .='
								</tr>
							</table>
						</div>
					</div>
					<script type="text/javascript">
						$(document).ready(function(){
							$(".eliminar").click(function(e){
								e.preventDefault();
								var id = $(this).attr("id");
								swal({
									title: "Atención!!!",
									text: "¿Esta seguro de inhabilitar el registro?!",
									type: "warning",
									showCancelButton: true,
									confirmButtonClass: "btn-danger",
									confirmButtonText: "Confirmar",
									cancelButtonText: "Cancelar",
									closeOnConfirm: false,
									closeOnCancel: false
								},
								function(isConfirm) {
									if (isConfirm) {
										//Si SE confirma la eliminacion se ejecuta el reenvio al php encargado
										window.location.href="index.php?c=cita&a=eliminar1&id="+id;
									} else {
										//Si se cancela se emite un mensaje
										swal("Cancelado", "Usted ha cancelado la acción de eliminación", "error");
									}
								});
							});
						});
					</script>';
				}
			}else{
				return $this->vista("error");
			}
		}*/

		/*public function listarCitasAjaxHoy(){
			if($this->accesoConsultar){
				$alm = new cita_model();
				$mensaje ="";
				date_default_timezone_set("America/Caracas");
				$fecha_actual = date("d-m-Y");
				$mensaje .='
					<div class="row">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3>Citas programadas para hoy '.$fecha_actual.'</h3>
							</div>
						</div>
					</div>
					<div class="col-md-12 text-center">
						<table class="table table-hover">
							<tr class="table-secondary">
								<th>Consultorio</th>
								<th>Turno</th>
								<th>Cédula</th>
								<th>Paciente</th>
								<th>Teléfono</th>
								<th>Correro</th>
								<th>Odontólogo</th>';
								if($this->accesoModificar){
									$mensaje .='<th>EDITAR</th>';
								}
								if($this->accesoEliminar){
									$mensaje .='<th>ELIMINAR</th>';
								}
								$mensaje .='
							</tr>';
							foreach ($this->mode->Consultar("listarCitasParaHoy") as $k){
								$mensaje .= '
									<tr>
										<td>'.$k->consultorio.'</td>
										<td>'.$k->turno.'</td>
										<td>'.$k->cedula.'</td>
										<td>'.$k->nombres.' '.$k->apellidos.'</td>
										<td>'.$k->tlfno.'</td>
										<td>'.$k->email.'</td>
										<td>'.$k->nombresDoctor.' '.$k->apellidosDoctor.'</td>';
										if($this->accesoModificar){
											$mensaje .='
											<td>
												<a href="index.php?c=cita&a=editar&id='.$k->id.'" type="button" class="btn btn-outline-primary">
													<i class="bi bi-pencil-square"></i>
												</a>
											</td>
											';
										}
										if($this->accesoEliminar){
											$mensaje .='
											<td>
												<button href="#" id="'.$k->id.'" type="button" class="btn btn-outline-danger eliminar">
													<i class="bi bi-trash"></i>
												</button>
											</td>
											';
										}
										$mensaje .='
									</tr>
								';
							}
						$mensaje .= '</table>
					</div>

					<script type="text/javascript">
						$(document).ready(function(){
							$(".eliminar").click(function(e){
								console.log();
								e.preventDefault();
								var id = $(this).attr("id");
								swal({
									title: "Atención!!!",
									text: "¿Esta seguro de eliminar el registro?!",
									type: "warning",
									showCancelButton: true,
									confirmButtonClass: "btn-danger",
									confirmButtonText: "Confirmar",
									cancelButtonText: "Cancelar",
									closeOnConfirm: false,
									closeOnCancel: false
									},
									function(isConfirm) {
										if (isConfirm) {
											//Si SE confirma la eliminacion se ejecuta el reenvio al php encargado
											window.location.href="index.php?c=cita&a=eliminar1&id="+id;
										} else {
											//Si se cancela se emite un mensaje
											swal("Cancelado", "Usted ha cancelado la acción de eliminación", "error");
										}
									}
								);
							});
						});
					</script>
				';
				echo $mensaje;
			}else{
				return $this->vista("error");
			}
		}*/
	}
?>