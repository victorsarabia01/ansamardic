<?php
include_once "conexion.php";
class proveedor_model extends conexion_database {

    //public $CNX;
    public $id;
	public $nombre;
    public $descripcion;
    public $direccion;
    public $email;
    public $stock;
    public $insumo;
    public $consulta;
    public $consulta1;
	public $status;
	public $tlfno;

 		//ENCAPSULAMIENTO DE FUNCIONES PRIVADAS ACCEDIDAS UNICAMENTE POR FUNCIONES PUBLICAS 
		public function Consultar($metodo, $param1=""){

			if($metodo=="listarProveedores"){ return self::listarProveedores(); }
			if($metodo=="listarStatus"){ return self::listarStatus(); }

			if($metodo=="cargarProveedor" && $param1!=""){ return self::cargarProveedor($param1); }
			if($metodo=="verificarProveedor" && $param1!=""){ return self::verificarProveedor($param1); }
			if($metodo=="buscarRegistroProveedor" && $param1!=""){ return self::buscarRegistroProveedor($param1); }
			if($metodo=="buscarRegistroInsumo" && $param1!=""){ return self::buscarRegistroInsumo($param1); }


			if($metodo=="verificarInsumoId" && $param1!=""){ return self::verificarInsumoId($param1); }
			if($metodo=="verificarInsumoNombre" && $param1!=""){ return self::verificarInsumoNombre($param1); }
			
		}
		public function Registrar($metodo, $param1=[]){
			if($metodo=="registrarProveedor" && !empty($param1)){ return self::registrarProveedor($param1); }
		}
		public function Modificar($metodo, $param1=[]){
			if($metodo=="modificarInsumo" && !empty($param1)){ return self::modificarInsumo($param1); }
		}
		public function Eliminar($metodo, $param1=""){
			if($metodo=="inhabilitarProveedor" && $param1!=""){ return self::inhabilitarProveedor($param1); }
		}

		//FUNCION PARA LLENAR TABLA PROVEEDOR
		private function listarProveedores(){
			try {
				
				$query="SELECT * from proveedor";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		private function cargarProveedor($id){
			try {
				$query="SELECT * FROM proveedor WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		

		private function verificarProveedor($descripcion){
			try {
				$query="SELECT * FROM proveedor WHERE descripcion=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($descripcion));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		private function registrarProveedor(proveedor_model $data){
				try {
					$query="INSERT into proveedor (descripcion,direccion,tlfno,email,status) values (?,?,?,?,?)";
					$this->CNX->prepare($query)->execute(array($data->descripcion,$data->direccion,$data->telefono,$data->email,'1'));
					
				} catch (Exception $e) {
					die($e->getMessage());
				}
			}

		private function buscarRegistroProveedor($consultaBusqueda){
			try {
				$query="SELECT * FROM proveedor WHERE descripcion=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($consultaBusqueda));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		private function inhabilitarProveedor($id){
			try {

				$query="UPDATE proveedor set status='0' WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
		}


		//MODIFICAR INSUMO EN BD
		private function modificarInsumo(insumo_model $data){
			try {
				$query="UPDATE insumo set nombre=?,descripcion=?,stock=?,status=? where id=?";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->descripcion,$data->stock,$data->status,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}


		// CARGAR REGISTRO DE CONSULTORIOS
		

		// VERIFICAR SI NOMBRE ESTA DISPONIBLE
		private function verificarInsumoId($id){
			try {
				$query="SELECT * FROM insumo WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		private function verificarInsumoNombre($nombre){
			try {
				$query="SELECT * FROM insumo WHERE nombre=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($nombre));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//FUNCION PARA COMPARAR STATUS EN LA VISTA EDITAR TIPO DE EMPLEADO
		private function listarStatus(){
			try {
				
				$query="SELECT * from status";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

}

?>