<?php
	
	class historia_model extends conexion {
		
		public $CNX;
	  	public $paciente;
		public $historia;
		public $fecha;
		public $id_paciente;
	
		public $bservacion;
		
		
		public function __construct(){
			try {
				$this->CNX = parent::conectar();
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		
		public function validarCedula($cedula){
			try {
				$query="SELECT id as consulta from paciente where cedula=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($cedula));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//RESGISTRAR EMPLEADO EN BD
		public function registrarhistoria(historia_model $data){
			try {
			    	$query="INSERT into historia (motivo,fecha,id_lesiones,id_tipo,observacion,id_tratamiento) VALUES (?,?,?,?,?,?)";
$this->CNX->prepare($query)->execute(array($data->motivo,$data->fecha,$data->id_lesiones,$data->dolor,$data->id_tipo,$data->observacion,$data->id_tratamiento));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}




		public function listarTodostipo(){
			try {
				
				$query="SELECT * from tipo";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarTodoslesiones(){
			try {
				
				$query="SELECT * from lesiones";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		public function listarTodostratamiento(){
			try {
				
				$query="SELECT * from tratamiento";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}


		
		public function listarhistoria(){
			try {
			   	
				  $query="SELECT p.id,p.cedula,p.nombres,p.apellidos,h.id_tratamiento,h.id_paciente,t.trabajo FROM paciente as p INNER JOIN historia as h 
				   JOIN tratamiento as t 
				   ON h.id=p.id";

					 
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		public function editarhistoria(){
			try {
			   	
				  $query="UPDATE historia SET motivo=value,fecha=value,id_paciente=value,toma_medicamento=value,id_lesiones=value,hablar=value,dolor=value,id_tipo=value,enfermedad=value,alergia=value,observacion=value,id_tratamiento=value<th>cedula</th>";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		
	
		
		
		//RESGISTRAR CONSULTORIO EN BD
		
		
	}
	
?>