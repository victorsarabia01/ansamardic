-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-11-2023 a las 01:44:18
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 8.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ansamar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos`
--

CREATE TABLE `accesos` (
  `id` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `id_permiso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `accesos`
--

INSERT INTO `accesos` (`id`, `id_rol`, `id_modulo`, `id_permiso`) VALUES
(240, 5, 1, 8),
(241, 5, 2, 7),
(242, 5, 2, 8),
(243, 5, 2, 9),
(244, 5, 2, 10),
(245, 5, 4, 7),
(246, 5, 4, 8),
(247, 5, 4, 9),
(248, 5, 4, 10),
(249, 5, 5, 7),
(250, 5, 5, 8),
(251, 5, 5, 9),
(252, 5, 5, 10),
(253, 5, 8, 7),
(254, 5, 8, 8),
(255, 5, 8, 9),
(256, 5, 8, 10),
(293, 4, 1, 8),
(294, 4, 2, 7),
(295, 4, 2, 8),
(296, 4, 3, 8),
(297, 3, 1, 7),
(298, 3, 1, 8),
(299, 3, 1, 9),
(300, 3, 1, 10),
(301, 3, 2, 7),
(302, 3, 2, 8),
(303, 3, 2, 9),
(304, 3, 2, 10),
(305, 3, 3, 7),
(306, 3, 3, 8),
(307, 3, 3, 9),
(308, 3, 3, 10),
(309, 3, 4, 7),
(310, 3, 4, 8),
(311, 3, 4, 9),
(312, 3, 4, 10),
(313, 3, 5, 7),
(314, 3, 5, 8),
(315, 3, 5, 9),
(316, 3, 5, 10),
(317, 3, 6, 7),
(318, 3, 6, 8),
(319, 3, 6, 9),
(320, 3, 6, 10),
(321, 3, 7, 7),
(322, 3, 7, 8),
(323, 3, 7, 9),
(324, 3, 7, 10),
(325, 3, 8, 7),
(326, 3, 8, 8),
(327, 3, 8, 9),
(328, 3, 8, 10),
(329, 3, 9, 7),
(330, 3, 9, 8),
(331, 3, 9, 9),
(332, 3, 9, 10),
(333, 3, 10, 7),
(334, 3, 10, 8),
(335, 3, 10, 9),
(336, 3, 10, 10),
(337, 3, 11, 7),
(338, 3, 11, 8),
(339, 3, 11, 9),
(340, 3, 11, 10),
(341, 3, 12, 7),
(342, 3, 12, 8),
(343, 3, 12, 9),
(344, 3, 12, 10),
(345, 3, 13, 7),
(346, 3, 13, 8),
(347, 3, 13, 9),
(348, 3, 13, 10),
(349, 3, 14, 7),
(350, 3, 14, 8),
(351, 3, 14, 9),
(352, 3, 14, 10),
(353, 3, 15, 7),
(354, 3, 15, 8),
(355, 3, 15, 9),
(356, 3, 15, 10),
(357, 3, 16, 7),
(358, 3, 16, 8),
(359, 3, 16, 9),
(360, 3, 16, 10),
(361, 3, 17, 7),
(362, 3, 17, 8),
(363, 3, 17, 9),
(364, 3, 17, 10),
(365, 3, 22, 7),
(366, 3, 22, 8),
(367, 3, 22, 9),
(368, 3, 22, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacion_insumo`
--

CREATE TABLE `asignacion_insumo` (
  `id` int(11) NOT NULL,
  `id_insumo` int(11) NOT NULL,
  `id_consultorio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_mes` int(11) NOT NULL,
  `id_consultorio` int(11) NOT NULL,
  `id_turno` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`id`, `fecha`, `id_paciente`, `id_mes`, `id_consultorio`, `id_turno`, `id_empleado`) VALUES
(55, '2023-08-28', 42, 8, 3, 2, 23),
(90, '2023-10-31', 42, 10, 1, 1, 21),
(91, '2023-11-01', 42, 11, 1, 1, 21),
(92, '2023-11-08', 42, 11, 1, 1, 21),
(93, '2023-11-15', 42, 11, 1, 1, 21),
(94, '2023-11-29', 42, 11, 1, 1, 21),
(95, '2023-10-31', 1, 10, 1, 1, 21),
(96, '2023-12-06', 42, 12, 1, 1, 21),
(97, '2023-03-08', 3, 3, 1, 1, 21),
(98, '2023-04-22', 27, 4, 1, 1, 21),
(99, '2023-05-05', 13, 5, 1, 1, 21),
(100, '2023-06-06', 13, 6, 1, 1, 21),
(101, '2023-12-02', 44, 12, 2, 2, 21),
(102, '2023-11-06', 42, 11, 2, 1, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion_medica`
--

CREATE TABLE `condicion_medica` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `observacion` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `condicion_medica`
--

INSERT INTO `condicion_medica` (`id`, `descripcion`, `observacion`, `status`) VALUES
(7, 'Hipertensión arterial', 'cuidado en cirugia', 1),
(8, 'Diabetes', 'Cuidado en cirugia', 1),
(9, 'VIH', 'Cuidado en cirugia', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultorio`
--

CREATE TABLE `consultorio` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `tlfno` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `sillas` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `consultorio`
--

INSERT INTO `consultorio` (`id`, `descripcion`, `direccion`, `tlfno`, `sillas`, `status`) VALUES
(1, 'Cerritos Blancos', 'oeste', '02514460589', 1, 0),
(2, 'Centro', 'Centro', '02514460689', 1, 1),
(3, 'Garabatal', 'Av el Garabatal', '02514460688', 1, 1),
(50, 'pruebaa', 'wewew', '544554', 1, 1),
(51, 'prueba', 'wewew', '544554', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_condicion_medica`
--

CREATE TABLE `detalles_condicion_medica` (
  `id` int(11) NOT NULL,
  `id_condicion_medica` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalles_condicion_medica`
--

INSERT INTO `detalles_condicion_medica` (`id`, `id_condicion_medica`, `id_paciente`) VALUES
(9, 9, 1),
(10, 7, 2),
(11, 7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalles_consulta`
--

CREATE TABLE `detalles_consulta` (
  `id` int(11) NOT NULL,
  `id_historia` int(11) NOT NULL,
  `id_servicio_dental` int(11) NOT NULL,
  `evolucion` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `observacion` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `indicaciones` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `detalles_consulta`
--

INSERT INTO `detalles_consulta` (`id`, `id_historia`, `id_servicio_dental`, `evolucion`, `observacion`, `indicaciones`, `status`) VALUES
(0, 43, 6, '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dia_semana`
--

CREATE TABLE `dia_semana` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `dia_semana`
--

INSERT INTO `dia_semana` (`id`, `nombre`) VALUES
(1, 'Lunes'),
(2, 'Martes'),
(3, 'Miercoles'),
(4, 'Jueves'),
(5, 'Viernes'),
(6, 'Sabado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `cedula` varchar(8) COLLATE utf8_spanish2_ci NOT NULL,
  `nombres` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `tlfno` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `id_tipo_empleado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `cedula`, `nombres`, `apellidos`, `fechaNacimiento`, `email`, `tlfno`, `direccion`, `status`, `id_tipo_empleado`) VALUES
(1, '00000000', 'Admin', 'System', '1880-01-01', 'admin_ansamar@gmail.com', '04160000000', 'Lara', 1, 1),
(21, '25630259', 'Angel Heliot', 'Perez Cardenas', '2000-05-01', 'angel_96@gmail.com', '04121507389', 'La Nueva Paz', 1, 3),
(22, '23835353', 'Aura Elena', 'Caldera Linares', '1994-01-03', 'auraelena2023@gmail.com', '04121788896', 'Carrera 7a entre 3 y 5 San Francisco', 1, 3),
(23, '9625722', 'Rosa Virginia', 'Perez', '1966-06-12', 'rosaperez016@gmail.com', '04145657177', 'La 37 San Juan', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `enfermedades`
--

CREATE TABLE `enfermedades` (
  `id` int(11) NOT NULL,
  `enfermedad` varchar(50) NOT NULL,
  `campo` varchar(25) NOT NULL,
  `precio_enfermedad` double NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `enfermedades`
--

INSERT INTO `enfermedades` (`id`, `enfermedad`, `campo`, `precio_enfermedad`, `status`) VALUES
(1, 'Fractura', 'Completa', 20, 1),
(2, 'Caries 1', 'Pieza', 15, 1),
(3, 'Caries 2', 'Completa', 20, 1),
(4, 'Caries 3', 'Pieza', 25, 1),
(5, 'Diastema', 'Pieza', 8.5, 1),
(6, 'Movilidad', 'Completa', 6, 1),
(7, 'Supernumerario', 'Pieza', 9, 1),
(8, 'Remanente Radicular', 'Pieza', 12, 1),
(9, 'Giroversión', 'Completa', 14, 1),
(10, 'Intrusión', 'Completa', 16, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historia`
--

CREATE TABLE `historia` (
  `id` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_cita` int(11) NOT NULL,
  `id_enfermedad` int(11) NOT NULL,
  `fecha` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `pieza_dental` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `posicion_dental` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `historia`
--

INSERT INTO `historia` (`id`, `id_paciente`, `id_cita`, `id_enfermedad`, `fecha`, `pieza_dental`, `posicion_dental`, `status`) VALUES
(43, 42, 55, 9, '2023-10-20', 'D16', 'OCLUSAL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `insumo`
--

CREATE TABLE `insumo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `insumo`
--

INSERT INTO `insumo` (`id`, `nombre`, `descripcion`, `stock`, `status`) VALUES
(3, 'Resina', 'resina', 12, 1),
(4, 'Amalgana', 'Amalgama', 100, 1),
(5, 'Suturas', 'suturas', 5, 0),
(6, 'asas', 'asas', 2, 1),
(7, 'resinaa', 'asas', 0, 1),
(8, 'prueba12', 'as', 0, 1),
(9, 'sads', 'as', 0, 1),
(10, 'prueba', 'prueba', 100, 1),
(11, 'prueba1', 'asas', 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `meses`
--

CREATE TABLE `meses` (
  `id` int(11) NOT NULL,
  `mes` varchar(20) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `meses`
--

INSERT INTO `meses` (`id`, `mes`) VALUES
(1, 'enero'),
(2, 'febrero'),
(3, 'marzo'),
(4, 'abril'),
(5, 'mayo'),
(6, 'junio'),
(7, 'julio'),
(8, 'agosto'),
(9, 'septiembre'),
(10, 'octubre'),
(11, 'noviembre'),
(12, 'diciembre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE `modulo` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modulo`
--

INSERT INTO `modulo` (`id`, `nombre`, `status`) VALUES
(1, 'Planificacion', 1),
(2, 'Cita', 1),
(3, 'Consultorio', 1),
(4, 'Paciente', 1),
(5, 'Condicion Medica', 1),
(6, 'Empleado', 1),
(7, 'Tipo De Empleado', 1),
(8, 'Odontograma', 1),
(9, 'Servicio Dental', 1),
(10, 'Consulta', 1),
(11, 'Insumos', 1),
(12, 'Reportes', 1),
(13, 'Usuario', 1),
(14, 'Bitacora', 1),
(15, 'Rol', 1),
(16, 'Modulo', 1),
(17, 'Permiso', 1),
(22, 'Insumo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `id` int(11) NOT NULL,
  `cedula` varchar(8) COLLATE utf8_spanish2_ci NOT NULL,
  `nombres` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `fechaNacimiento` date NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `tlfno` varchar(11) COLLATE utf8_spanish2_ci NOT NULL,
  `sexo` varchar(1) COLLATE utf8_spanish2_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`id`, `cedula`, `nombres`, `apellidos`, `fechaNacimiento`, `direccion`, `email`, `tlfno`, `sexo`, `status`) VALUES
(1, '22186490', 'yurmi', 'figueroa', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '04245208619', 'F', 1),
(2, '22186491', 'dasfsdf', 'sdfsdf', '2023-07-31', '5454', 'asdasd', '56546546', '', 0),
(3, '45454', 'asdfasdf', 'asdfdsf', '2023-07-31', 'asdads', 'asdasd', '56546', '', 0),
(4, '21321321', 'asdfasdf', 'asdfdaf', '2023-07-31', 'asdasdsa', 'asdasd', '2622', '', 0),
(5, '54554', 'asasd', 'asdasd', '2023-08-08', 'asdfafaf', 'asdasdas', '5656546', '', 0),
(6, '45546546', 'sdfsdf', 'sdfsdfdf', '2023-07-30', 'sdfsdsdf', 'dfsdfsdf', '5454546', '', 0),
(7, '51654654', 'xdfdf', 'fdgdfg', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '545454', '', 0),
(8, '44545', 'adf', 'dafdf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '4545', '', 0),
(9, '544545', 'sadsad', 'asasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '4545', '', 0),
(10, '454545', 'asdsad', 'asasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '44545', '', 0),
(11, '454456', 'asdsad', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '455445', '', 0),
(12, '12564445', 'sdfdsf', 'sdfdsf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '4545645', '', 0),
(13, '456', 'dsf', 'sdf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '546', '', 0),
(14, '546456', 'sdfsdf', 'sdfsdf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '546456546', '', 0),
(15, '4456456', 'adsfasf', 'adfdaf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '4554', '', 0),
(16, '45654645', 'asdfdf', 'sdfdsf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '45', '', 0),
(17, '445456', 'dfdf', 'dfdsf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '454545', '', 0),
(18, '456546', 'adfsdf', 'dsfsdfds', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '465654', '', 0),
(19, '65546546', 'sdsadsad', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '5445', '', 0),
(20, '56454654', 'asdasd', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '4545', '', 0),
(21, '456456', 'asdasd', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '4545', '', 0),
(22, '51564456', 'afdsaf', 'adfdf', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '455454', '', 0),
(23, '121212', 'sadasd', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '55445', '', 0),
(24, '54645656', 'asdsad', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '54654', '', 1),
(25, '12345678', 'sdasd', 'asdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '546456456', '', 1),
(26, '4544545', 'asdasdsa', 'asasdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '454545', '', 1),
(27, '4545', 'asdasd', 'asasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '456456456', '', 1),
(28, '54654654', 'asdasdas', 'asdasasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '5465454', '', 1),
(29, '878787', 'asdasd', 'asdasdd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '8798797', '', 1),
(30, '545454', 'prueba', 'prueba', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '45546546546', '', 1),
(31, '54654546', 'asasdasd', 'asdasasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '5465546', '', 1),
(32, '454654', 'asdasdasd', 'asdasdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '54546546', '', 1),
(33, '54546546', 'ASDASDAS', 'ASDASDASD', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '5456546', '', 1),
(34, '54545465', 'ASDASASD', 'ASDASDASD', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '546546', '', 1),
(35, '5651621', 'sasdasdasda', 'asdasdasd', '1994-04-12', 'direccion', 'ejemplo@gmail.com', '212121', '', 1),
(36, '26565', 'asdasdasd', 'sadasdsad', '2000-01-01', 'asadssad', 'sdasd@gmail.com', '655546', 'm', 1),
(37, '211212', 'ASDASDDSAASDDAS', 'ASDASDADSADS', '1994-01-01', 'ASDASDASDSAD', 'asdsadad@gmail.com', '1212121212', 'm', 1),
(38, '2121212', 'asdasdasd', 'asdasdasdsad', '2006-01-01', 'sadasdasd', 'asdsadad@gmail.com', '654564545', 'm', 1),
(39, '4', 'f', 'd', '1111-02-02', 'dfdf', 'd@gmail.com', '3', 'm', 1),
(40, '1566752', 'sadasd', 'asdasd', '1994-04-12', 'direccion', 'LESNIGUEDEZ02@GMAIL.COM', '54545454', '', 1),
(41, '', '', '', '1994-04-12', 'direccion', '', '', '', 1),
(42, '6642014', 'Eliodoro ', 'Cardenas ', '1994-04-12', 'direccion', 'eliodorojose46@gmail.com', '04245669248', '', 1),
(43, '', '', '', '1994-04-12', 'direccion', '', '', '', 1),
(44, '9540060', 'Maribel', 'Duran', '1990-03-01', 'Calle 53 con carrera 6', 'maribelduran@hotmail.com', '04167899562', 'f', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permiso`
--

CREATE TABLE `permiso` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `permiso`
--

INSERT INTO `permiso` (`id`, `nombre`, `status`) VALUES
(7, 'Registrar', 1),
(8, 'Consultar', 1),
(9, 'Modificar', 1),
(10, 'Eliminar', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planificacion`
--

CREATE TABLE `planificacion` (
  `id` int(11) NOT NULL,
  `id_consultorio` int(11) NOT NULL,
  `id_turno` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_dia_semana` int(30) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `planificacion`
--

INSERT INTO `planificacion` (`id`, `id_consultorio`, `id_turno`, `id_empleado`, `id_dia_semana`, `status`) VALUES
(264, 3, 2, 21, 2, 0),
(265, 1, 1, 21, 6, 1),
(266, 2, 2, 22, 1, 1),
(267, 2, 1, 22, 6, 0),
(268, 3, 1, 22, 3, 0),
(269, 3, 2, 22, 3, 0),
(270, 3, 2, 23, 1, 0),
(271, 3, 2, 23, 2, 0),
(272, 3, 2, 23, 4, 0),
(273, 3, 2, 23, 5, 0),
(274, 3, 2, 23, 6, 0),
(275, 1, 1, 21, 2, 0),
(276, 1, 1, 21, 2, 0),
(277, 1, 1, 21, 3, 1),
(278, 1, 1, 21, 4, 1),
(279, 1, 1, 21, 5, 1),
(280, 2, 2, 21, 6, 1),
(281, 2, 1, 22, 1, 1),
(282, 3, 1, 23, 5, 1),
(283, 3, 2, 23, 6, 1),
(284, 2, 2, 23, 2, 1),
(285, 2, 2, 23, 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`, `status`) VALUES
(3, 'Superusuario', 1),
(4, 'Prueba', 1),
(5, 'Asistente', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_dental`
--

CREATE TABLE `servicio_dental` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `precio` double NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `servicio_dental`
--

INSERT INTO `servicio_dental` (`id`, `nombre`, `descripcion`, `precio`, `status`) VALUES
(1, 'Corona Temporal', 'asd', 15, 1),
(2, 'Corona Completa', 'curar el diente afectado, llenando la via con resina', 75, 1),
(3, 'Corona Veerner', 'asd', 30, 1),
(4, 'Corona Fexestrada', 'prueba', 12, 1),
(5, 'Corona 3/4', 'asd', 15, 1),
(6, 'Corona de Porcelana', 'asd', 10, 1),
(7, 'Protesis fija', 'asd', 11, 1),
(8, 'Protesis Removible', 'asd', 18, 1),
(9, 'Endodoncia', 'asd', 22, 1),
(10, 'Amalgama', 'asd', 9, 1),
(11, 'Diente por extraer', 'asd', 15, 1),
(12, 'Resina', 'asd', 18, 1),
(13, 'Microdoncia', 'asd', 25, 1),
(14, 'Macrodoncia', 'asd', 25, 1),
(15, 'Extracción', 'asd', 30, 1),
(16, 'Limpieza', 'asd', 20, 1),
(17, 'dsf', '', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `status` varchar(8) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `status`) VALUES
(0, 'Inactivo'),
(1, 'Activo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_empleado`
--

CREATE TABLE `tipo_empleado` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `tipo_empleado`
--

INSERT INTO `tipo_empleado` (`id`, `descripcion`, `status`) VALUES
(1, 'Administrador', 1),
(2, 'Asistente', 1),
(3, 'Odontologo', 1),
(9, '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `descripcion`) VALUES
(1, 'Mañana'),
(2, 'Tarde');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `id_empleado` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(250) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `id_empleado`, `id_rol`, `usuario`, `password`, `status`) VALUES
(1, 1, 3, 'Admin', 'Admin.2023', 1),
(4, 22, 5, 'Aura', '123456', 1),
(5, 21, 4, 'Angel', '123456', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesos`
--
ALTER TABLE `accesos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_rol` (`id_rol`),
  ADD KEY `fk_id_modulo` (`id_modulo`),
  ADD KEY `fk_id_permiso` (`id_permiso`);

--
-- Indices de la tabla `asignacion_insumo`
--
ALTER TABLE `asignacion_insumo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_insumo` (`id_insumo`),
  ADD KEY `fk_id_consultorio_asignacion` (`id_consultorio`);

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_paciente` (`id_paciente`),
  ADD KEY `fk_id_consultorio2` (`id_consultorio`),
  ADD KEY `fk_id_turno2` (`id_turno`),
  ADD KEY `fk_id_empleado2` (`id_empleado`),
  ADD KEY `id_mes2` (`id_mes`);

--
-- Indices de la tabla `condicion_medica`
--
ALTER TABLE `condicion_medica`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `consultorio`
--
ALTER TABLE `consultorio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_status` (`status`);

--
-- Indices de la tabla `detalles_condicion_medica`
--
ALTER TABLE `detalles_condicion_medica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_condicion_medica` (`id_condicion_medica`),
  ADD KEY `fk_id_paciente_2` (`id_paciente`);

--
-- Indices de la tabla `detalles_consulta`
--
ALTER TABLE `detalles_consulta`
  ADD KEY `id_historia` (`id_historia`),
  ADD KEY `id_servicio_dental` (`id_servicio_dental`);

--
-- Indices de la tabla `dia_semana`
--
ALTER TABLE `dia_semana`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tipo_empleado` (`id_tipo_empleado`),
  ADD KEY `fk_id_status` (`status`);

--
-- Indices de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `historia`
--
ALTER TABLE `historia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_paciente` (`id_paciente`),
  ADD KEY `fk_id_cita` (`id_cita`),
  ADD KEY `fk_id_enfermedad` (`id_enfermedad`);

--
-- Indices de la tabla `insumo`
--
ALTER TABLE `insumo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `meses`
--
ALTER TABLE `meses`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_status2` (`status`);

--
-- Indices de la tabla `permiso`
--
ALTER TABLE `permiso`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `planificacion`
--
ALTER TABLE `planificacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_id_empelado` (`id_empleado`),
  ADD KEY `fk_id_consultorio` (`id_consultorio`),
  ADD KEY `fk_dia_semana` (`id_dia_semana`),
  ADD KEY `fk_id_turno` (`id_turno`),
  ADD KEY `fk_id_status1` (`status`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicio_dental`
--
ALTER TABLE `servicio_dental`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_empleado`
--
ALTER TABLE `tipo_empleado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_empleado` (`id_empleado`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesos`
--
ALTER TABLE `accesos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT de la tabla `asignacion_insumo`
--
ALTER TABLE `asignacion_insumo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `condicion_medica`
--
ALTER TABLE `condicion_medica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `consultorio`
--
ALTER TABLE `consultorio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `detalles_condicion_medica`
--
ALTER TABLE `detalles_condicion_medica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `dia_semana`
--
ALTER TABLE `dia_semana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `enfermedades`
--
ALTER TABLE `enfermedades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `historia`
--
ALTER TABLE `historia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `insumo`
--
ALTER TABLE `insumo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `meses`
--
ALTER TABLE `meses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `modulo`
--
ALTER TABLE `modulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `paciente`
--
ALTER TABLE `paciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `permiso`
--
ALTER TABLE `permiso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `planificacion`
--
ALTER TABLE `planificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `servicio_dental`
--
ALTER TABLE `servicio_dental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_empleado`
--
ALTER TABLE `tipo_empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `accesos`
--
ALTER TABLE `accesos`
  ADD CONSTRAINT `fk_id_modulo` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_permiso` FOREIGN KEY (`id_permiso`) REFERENCES `permiso` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_rol` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `asignacion_insumo`
--
ALTER TABLE `asignacion_insumo`
  ADD CONSTRAINT `fk_id_consultorio_asignacion` FOREIGN KEY (`id_consultorio`) REFERENCES `consultorio` (`id`),
  ADD CONSTRAINT `fk_id_insumo` FOREIGN KEY (`id_insumo`) REFERENCES `insumo` (`id`);

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `fk_id_consultorio2` FOREIGN KEY (`id_consultorio`) REFERENCES `consultorio` (`id`),
  ADD CONSTRAINT `fk_id_empleado2` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id`),
  ADD CONSTRAINT `fk_id_turno2` FOREIGN KEY (`id_turno`) REFERENCES `turno` (`id`),
  ADD CONSTRAINT `id_mes2` FOREIGN KEY (`id_mes`) REFERENCES `meses` (`id`),
  ADD CONSTRAINT `id_paciente2` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id`);

--
-- Filtros para la tabla `detalles_condicion_medica`
--
ALTER TABLE `detalles_condicion_medica`
  ADD CONSTRAINT `fk_id_condicion_medica` FOREIGN KEY (`id_condicion_medica`) REFERENCES `condicion_medica` (`id`),
  ADD CONSTRAINT `fk_id_paciente_2` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id`);

--
-- Filtros para la tabla `detalles_consulta`
--
ALTER TABLE `detalles_consulta`
  ADD CONSTRAINT `id_historia` FOREIGN KEY (`id_historia`) REFERENCES `historia` (`id`),
  ADD CONSTRAINT `id_servicio_dental` FOREIGN KEY (`id_servicio_dental`) REFERENCES `servicio_dental` (`id`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fk_tipo_empleado` FOREIGN KEY (`id_tipo_empleado`) REFERENCES `tipo_empleado` (`id`);

--
-- Filtros para la tabla `historia`
--
ALTER TABLE `historia`
  ADD CONSTRAINT `fk_id_cita` FOREIGN KEY (`id_cita`) REFERENCES `cita` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_enfermedad` FOREIGN KEY (`id_enfermedad`) REFERENCES `enfermedades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_paciente` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `planificacion`
--
ALTER TABLE `planificacion`
  ADD CONSTRAINT `fk_dia_semana` FOREIGN KEY (`id_dia_semana`) REFERENCES `dia_semana` (`id`),
  ADD CONSTRAINT `fk_id_consultorio` FOREIGN KEY (`id_consultorio`) REFERENCES `consultorio` (`id`),
  ADD CONSTRAINT `fk_id_empelado` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id`),
  ADD CONSTRAINT `fk_id_turno` FOREIGN KEY (`id_turno`) REFERENCES `turno` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_id_empleado1` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_rol1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
